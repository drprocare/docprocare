<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  

<!-- Mirrored from htmlstream.com/preview/unify-v1.8/blog_large_full_width_item1.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Jan 2016 17:13:09 GMT -->
<head>
   <title>Drprocare | Blog</title> 

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">

    <!-- Web Fonts -->
    <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin'>

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">

    <!-- CSS Header and Footer -->
    <link rel="stylesheet" href="assets/css/headers/header-default.css">
    <link rel="stylesheet" href="assets/css/footers/footer-v1.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="assets/plugins/animate.css">
    <link rel="stylesheet" href="assets/plugins/line-icons/line-icons.css">
    <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/plugins/sky-forms-pro/skyforms/css/sky-forms.css">
    <link rel="stylesheet" href="assets/plugins/sky-forms-pro/skyforms/custom/custom-sky-forms.css">

    <!-- CSS Theme -->
    <link rel="stylesheet" href="assets/css/theme-colors/default.css" id="style_color">
    <link rel="stylesheet" href="assets/css/theme-skins/dark.css">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="assets/css/custom.css">

    <?php include 'head.php';   ?>
</head>	

<body>
   

<div class="wrapper">
    <?php include 'header.php';  ?>
    <!--=== End Header ===-->


    <!--=== Blog Posts ===-->
    <div class="bg-color-light">
        <div class="container content-sm">
            <!-- News v3 -->
            <div class="news-v3 bg-color-white margin-bottom-30">
                <img class="img-responsive full-width" src="assets/img/blog/1.jpg" alt="">
                <div class="news-v3-in">
                    <ul class="list-inline posted-info">
                        <li>By <a href="#">Drprocare</a></li>
                        <li>In <a href="#">Design</a></li>
                        <!-- li>Posted January 24, 2015</li>
 -->                    </ul>
                    <h2><a href="#">WHY AYURVEDA?</a></h2>
                    <p>Ayurveda is a 5,000-year-old system of natural healing that has its origins in the Vedic culture of India. Although suppressed during years of foreign occupation, Ayurveda has been enjoying a major resurgence in both its native land and throughout the world. Tibetan medicine and Traditional Chinese Medicine both have their roots in Ayurveda. Early Greek medicine also embraced many concepts originally described in the classical Ayurvedic medical texts dating back several thousands of years.</p>

                    <p>More than a mere system of treating illness, Ayurveda is a science of life (Ayur = life, Veda = science or knowledge). It offers a body of wisdom designed to help people stay vital while realizing their full human potential. Providing guidelines on ideal daily and seasonal routines, diet, behavior and the proper use of our senses, Ayurveda reminds us that health is the balanced and dynamic integration between our environment, body, mind, and spirit.</p>
                    <blockquote class="hero">
                        <p><em>"In Ayurveda, body, mind and consciousness work together in maintaining balance. They are simply viewed as different facets of one’s being. To learn how to balance the body, mind and consciousness requires an understanding of how vata, pitta and kapha work together."</em></p>
                    </blockquote>
                    <p>According to Ayurvedic philosophy the entire cosmos is an interplay of the energies of the five great elements—Space, Air, Fire, Water and Earth. Vata, pitta and kapha are combinations and permutations of these five elements that manifest as patterns present in all creation. In the physical body, vata is the subtle energy of movement, pitta the energy of digestion and metabolism, and kapha the energy that forms the body’s structure.</p>
                    <p><b>Benefits of Ayurvedic Medicine</b></p>
                    <p>Helps Lower Stress and Anxiety</p>
                    <p>Lowers Blood Pressure and Cholesterol</p> 
                    <p>Helps with Recovery from Injuries and Illnesses</p>
                    <p>Promotes a Nutrient-Dense, Antioxidant-Rich Diet</p>
                    <p>Can Help with Weight Loss or Maintenance</p>
                    <p>Lowers Inflammation</p>
                    <p>Helps with Hormonal Balance</p>                   
                    <ul class="post-shares post-shares-lg">
                        <li>
                            <a href="#">
                                <i class="rounded-x icon-speech"></i>
                                <span></span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="rounded-x icon-share"></i>
                                <span></span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="rounded-x icon-heart"></i>
                                <span></span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>                        
            <!-- End News v3 -->

          
        </div><!--/end container-->

        <!--=== Blog Posts ===-->
    <div class="bg-color-light">
        <div class="container content-sm">
            <!-- News v3 -->
            <div class="news-v3 bg-color-white margin-bottom-30">
                <img class="img-responsive full-width" src="assets/img/blog/3.jpg" alt="">
                <div class="news-v3-in">
                    <ul class="list-inline posted-info">
                        <li>By <a href="#">Drprocare</a></li>
                        <li>In <a href="#">Design</a></li>
                        <!-- li>Posted January 24, 2015</li>
 -->                    </ul>
                    <h2><a href="#">MENTAL HEALTH</a></h2>
                    <p>Mental health includes our emotional, psychological, and social well-being. It affects how we think,feel, and act. It also helps determine how we handle stress, relate to others, and make choices.Mental health is important at every stage of life, from childhood and adolescence through adulthood.Over the course of your life, if you experience mental health problems, your thinking, mood, and behavior could be affected. Many factors contribute to mental health problems.</p>

                    <p><b>There are five major categories of mental illnesses:</b></p>
                    <p>Anxiety disorders.</p>
                    <p>Mood disorders.</p>
                    <p>Schizophrenia and psychotic disorders.</p>
                    <p>Dementia.</p>
                    <p>Eating disorders.</p>
                    <p>Bipolar and related disorders</p>
                    <p>Trauma- and stressor-related disorders</p>
                
                    <p><b>Mental Health Disorders are More Common in Children and Young People than Many Realize</b></p>
                    <p>Studies show that at least 1 in 5 children and adolescents have a mental health disorder at any given time. Yet, fewer than one in five of these children receive the mental health services they need. Among young people, at least 1 in every 10 has a serious emotional disturbance at any given time.World Mental Health Day is a good reminder that it is important to talk about mental health. It’s more common than you may think.</p>   

                    <p>It’s important to know that recovery from most behavioral health problems is possible. Most people with a mental illness recover well with appropriate ongoing treatment and support. If you or a loved one is suffering from a behavioral health problem, please talk to someone. People with mental illness severe enough to cause disability are able to live independently in the community, if given the opportunity and support to do so.</p>

                    <p><b>DIY:</b></p>
                    <p>In most cases, a mental illness won't get better if you try to treat it on your own without professional care. But you can do some things for yourself that will build on your treatment plan:</p>

                    <p><b>Stay active.</b> Exercise can help you manage symptoms of depression, stress and anxiety.Physical activity can also counteract the effects of some psychiatric medications that may cause weight gain. Consider walking, swimming, gardening or any form of physical activity that you enjoy. Even light physical activity can make a difference.</p>

                    <p><b>Stick to your treatment plan.</b> Don't skip therapy sessions. Even if you're feeling better, don't skip your medications. If you stop, symptoms may come back. And you could have withdrawal-like symptoms if you stop a medication too suddenly. If you have bothersome drug side effects or other problems with treatment, talk to your doctor before making changes.</p>

                    <p><b>Determine priorities.</b> You may reduce the impact of your mental illness by managing time and energy. Cut back on obligations when necessary and set reasonable goals. Give yourself permission to do less when symptoms are worse. You may find it helpful to make a list of daily tasks or use a planner to structure your time and stay organized.</p>

                    <p><b>Avoid alcohol and drug use.</b> Using alcohol or recreational drugs can make it difficult to treat a mental illness. If you're addicted, quitting can be a real challenge. If you can't quit on your own, see your doctor or find a support group to help you.</p>

                    <p><b>Don't make important decisions when your symptoms are severe.</b> Avoid decision-making when you're in the depth of mental illness symptoms, since you may not be thinking clearly.</p>

                    <ul class="post-shares post-shares-lg">
                        <li>
                            <a href="#">
                                <i class="rounded-x icon-speech"></i>
                                <span></span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="rounded-x icon-share"></i>
                                <span></span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="rounded-x icon-heart"></i>
                                <span></span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>                        
            <!-- End News v3 -->

          
        </div><!--/end container-->


         <!--=== Blog Posts ===-->
    <div class="bg-color-light">
        <div class="container content-sm">
            <!-- News v3 -->
            <div class="news-v3 bg-color-white margin-bottom-30">
                <img class="img-responsive full-width" src="assets/img/blog/2.jpg" alt="">
                <div class="news-v3-in">
                    <ul class="list-inline posted-info">
                        <li>By <a href="#">Drprocare</a></li>
                        <li>In <a href="#">Design</a></li>
                        <!-- li>Posted January 24, 2015</li>
 -->                    </ul>
                    <h2><a href="#">Ebola virus disease</a></h2>

                    <p><b>WHAT IS EBOLA VIRUS DISEASE?</b></p>
                    <p>Ebola Virus Disease (EVD) or Ebola hemorrhagic fever is a rare and deadly disease in people and nonhuman primates. The viruses that cause EVD are located mainly in sub-Saharan Africa. People can get EVD through direct contact with an infected animal (bat or nonhuman primate) or a sick or dead person infected with Ebola virus.There is no approved vaccine or treatment for EVD. Research on EVD focuses on finding the virus’ natural host, developing vaccines to protect at-risk populations, and discovering therapies to improve treatment of the disease.Ebola Virus Disease (EVD) is a rare and deadly disease most commonly affecting people and nonhuman primates (monkeys, gorillas, and chimpanzees). It is caused by an infection with a group of viruses within the genus Ebolavirus.</p>

                    <p><b>HISTORY OF EBOLA VIRUS:</b></p>
                    <p>Ebola virus disease (EVD), one of the deadliest viral diseases, was discovered in 1976 when two consecutive outbreaks of fatal hemorrhagic fever occurred in different parts of Central Africa. The first outbreak occurred in the Democratic Republic of Congo (formerly Zaire) in a village near the Ebola River, which gave the virus its name. The second outbreak occurred in what is now South Sudan, approximately 500 miles (850 km) away.Initially, public health officials assumed these outbreaks were a single event associated with an infected person who traveled between the two locations. However, scientists later discovered that the two outbreaks were caused by two genetically distinct viruses: Zaire ebolavirus and Sudan ebolavirus. After this discovery, scientists concluded that the virus came from two different sources and spread independently to people in each of the affected areas.</p>
                
                    <p><b>SYMPTOMS OF EBOLA:</b></p>
                    <p><b>The time interval from infection with Ebola to the onset of symptoms is 2-21 days, although 8-10 days is most common. Signs and symptoms include:</b></p>   
                    <p>1)Fever and headache</p>
                    <p>2)joint and muscle aches</p>
                    <p>3)weakness</p>
                    <p>4)diarrhea</p>
                    <p>5)vomiting</p>
                    <p>6)stomach pain</p>
                    <p>7)lack of appetite</p>
                    <p>8)cough and sore throat</p>
                    <p>9)chest pain</p>
                    <p>10)difficulty breathing and swallowing</p>

                    <p><b>PREVENTION:</b></p>
                    <p>Funeral or burial rituals that require handling the body of someone who died from EVD.</p>   
                    <p>Contact with blood and body fluids (such as urine, feces, saliva, sweat)</p>
                    <p>Contact with bats and nonhuman primates or blood, fluids and raw meat prepared from these animals or meat from an unknown source.</p>
                    

                    <ul class="post-shares post-shares-lg">
                        <li>
                            <a href="#">
                                <i class="rounded-x icon-speech"></i>
                                <span></span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="rounded-x icon-share"></i>
                                <span></span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="rounded-x icon-heart"></i>
                                <span></span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>                        
            <!-- End News v3 -->

          
        </div><!--/end container-->
    </div>
    <!--=== End Blog Posts ===-->

    <!--=== Footer Version 1 ===-->
    <?php include 'footer.php';  ?>
    <!--=== End Footer Version 1 ===-->
</div><!--/wrapper-->

<!-- JS Global Compulsory -->			
<script type="text/javascript" src="assets/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="assets/plugins/jquery/jquery-migrate.min.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- JS Implementing Plugins -->
<script type="text/javascript" src="assets/plugins/back-to-top.js"></script>
<script type="text/javascript" src="assets/plugins/smoothScroll.js"></script>
<script type="text/javascript" src="assets/plugins/sky-forms-pro/skyforms/js/jquery.form.min.js"></script>
<script type="text/javascript" src="assets/plugins/sky-forms-pro/skyforms/js/jquery.validate.min.js"></script>
<!-- JS Customization -->
<script type="text/javascript" src="assets/js/custom.js"></script>
<!-- JS Page Level -->           
<script type="text/javascript" src="assets/js/app.js"></script>
<script type="text/javascript" src="assets/js/forms/login.js"></script>
<script type="text/javascript" src="assets/js/forms/contact.js"></script>
<script type="text/javascript" src="assets/js/plugins/style-switcher.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
      	App.init();
        LoginForm.initLoginForm();
        ContactForm.initContactForm();
        StyleSwitcher.initStyleSwitcher();      
    });
</script>
<!--[if lt IE 9]>
    <script src="assets/plugins/respond.js"></script>
    <script src="assets/plugins/html5shiv.js"></script>
    <script src="assets/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->

</body>

<!-- Mirrored from htmlstream.com/preview/unify-v1.8/blog_large_full_width_item1.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Jan 2016 17:13:09 GMT -->
</html>