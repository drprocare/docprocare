<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  

<!-- Mirrored from htmlstream.com/preview/unify-v1.8/page_home7.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Jan 2016 17:09:10 GMT -->
<head>
   <title>Drprocare | BMI</title>     

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">

<?php include 'head.php';  ?>
<link rel="stylesheet" href="assets/plugins/sky-forms-pro/skyforms/css/sky-forms.css">
<link rel="stylesheet" href="assets/plugins/sky-forms-pro/skyforms/custom/custom-sky-forms.css">
<link rel="stylesheet" href="assets/plugins/hover-effects/css/custom-hover-effects.css">
   
</head> 

<body>


<div class="wrapper">
    <!--=== Header ===-->    
    <?php include 'header.php'; ?>
    <!--=== End Header ===-->

    <!--=== Slider ===-->
    
	<!--=== End Slider ===-->

    <!--=== Content Part ===-->
    <div class="container content">
        <!-- Begin Service Block -->
        <div class="row margin-bottom-40">
			
			<div class="shadow-wrapper">
				<div class="tag-box tag-box-v1 box-shadow shadow-effect-2 col-md-12">
					<div class="col-md-3">
						<div class="heading heading-v1 ">
							<h2><i class="icon-custom rounded-x icon-bg-dark-blue icon-lin">1</i></h2>
							<p><h3>You are</h3></p>
							<br/>
							<p>
								<a href="#" class="" type="button"><i class="icon-custom icon-lg rounded-x icon-bg-dark-blue icon-line icon-user"></i></a>
								&nbsp;&nbsp;
								<a href="#" class="" type="button"><i class="icon-custom icon-lg rounded-x icon-bg-dark-blue icon-line icon-user-female"></i></a>
							</p>
						</div>
					</div>
					<div class="col-md-3">
						<div class="heading heading-v1 ">
							<h2><i class="icon-custom rounded-x icon-bg-dark-blue icon-lin">2</i></h2>
							<p><h3>How old are you?</h3></p>
							<br/><br/>
							<p class="input-group">
								<input type="text" class="form-control">
								<span class="input-group-addon badge-blue" style="color:white;">Year</span>
							</p>
						</div>
					</div>
					<div class="col-md-3">
						<div class="heading heading-v1">
							<h2><i class="icon-custom rounded-x icon-bg-dark-blue icon-lin">3</i></h2>
							<p><h3>How tall are you?</h3></p>
							<br/><br/>
							<p class="input-group">
								<input type="text" class="form-control">
								<span class="input-group-addon badge-blue" style="color:white;">CM</span>
							</p>
						</div>
					</div>
					<div class="col-md-3">
						<div class="heading heading-v1 ">
							<h2><i class="icon-custom rounded-x icon-bg-dark-blue icon-lin">4</i></h2>
							<p><h3>How much do you weight?</h3></p>
							<br/><br/>
							<p class="input-group">
								<input type="text" class="form-control">
								<span class="input-group-addon badge-blue" style="color:white;">KG</span>
							</p>
						</div>
					</div>
					
					<div class="col-md-12">
						<center><a rel="grow-rotate" class="btn-u btn-u-blue grow-rotate">Calculate Your BMI</a></center>
					</div>
				</div>
			</div>
        </div>
       
    </div>
    
    <?php include 'footer.php';  ?>

</body>

<!-- Mirrored from htmlstream.com/preview/unify-v1.8/page_home7.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Jan 2016 17:09:16 GMT -->
</html> 