<div class="footer-v1">
        <div class="footer">
            <div class="container">
                <div class="row">
                    <!-- About -->
                    <div class="col-md-3 md-margin-bottom-40">
                        <a href="index-2.html"><img id="logo-footer" class="footer-logo" src="assets/img/logo.png" alt="" height="100px" width="180px"></a>
                        <p>Dr.procare is developed for patients, doctors, hospitals and pharmacies focused on being effortless positively impact experience which provides online appointment booking, lab test from home,blood orderinf and donating, online ordering medicine and so on.</p>
                           
                    </div><!--/col-md-3-->
                    <!-- End About -->

                    <!-- Latest -->
                     <div class="col-md-3 md-margin-bottom-40">
                        <div class="headline"><h2>Fitness & Welness</h2></div>
                        <ul class="list-unstyled link-list">
                            <li><a href="weight_manage.php">Weight Management</a><i class="fa fa-angle-right"></i></li>
                            <li><a href="dietveg.php">Diet Chart</a><i class="fa fa-angle-right"></i></li>
                            <li><a href="quick_firstaid.php">Quick First-aid</a><i class="fa fa-angle-right"></i></li>
                            <li><a href="workout_session.php">Workout Session</a><i class="fa fa-angle-right"></i></li>
                            
                        </ul>
                    </div><!--/col-md-3-->  
                    <!-- End Latest --> 
                    
                    <!-- Link List -->
                    <div class="col-md-3 md-margin-bottom-40">
                        <div class="headline"><h2>Useful Links</h2></div>
                        <ul class="list-unstyled link-list">
                            <li><a href="index.php">Home</a><i class="fa fa-angle-right"></i></li>
                            <li><a href="hospital.php">Hospital</a><i class="fa fa-angle-right"></i></li>
                            <li><a href="medical_store.php">Medical Store</a><i class="fa fa-angle-right"></i></li>
                            <li><a href="blood_bank.php">Blood Bank</a><i class="fa fa-angle-right"></i></li>
                            <li><a href="contact.php">Contact us</a><i class="fa fa-angle-right"></i></li>
                        </ul>
                    </div><!--/col-md-3-->
                    <!-- End Link List -->                    

                    <!-- Address -->
                    <div class="col-md-3 map-img md-margin-bottom-40">
                        <div class="headline"><h2>Contact Us</h2></div>                         
                        <address class="md-margin-bottom-40">
                            3rd Floor Shraddha Plaza, <br />
                            Kosamba, Gujarat 394120 <br />
                            Phone: 7622884442 <br />
                            Email: <a href="mailto:info@anybiz.com" class="">drprocare@gmail.com</a>
                        </address>
                    </div><!--/col-md-3-->
                    <!-- End Address -->
                </div>
            </div> 
        </div><!--/footer-->

        <div class="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">                     
                        <!-- <p>
                            2015 &copy; All Rights Reserved. Unify Theme by 
                            <a target="_blank" href="https://twitter.com/htmlstream">Htmlstream</a> | <a href="#">Privacy Policy</a> | <a href="#">Terms of Service</a>
                        </p> -->
                    </div>
                    <!-- Social Links -->
                    <div class="col-md-6">  
                        <ul class="social-icons pull-right">
                            <li><a href="#" data-original-title="Facebook" class="rounded-x social_facebook"></a></li>
                            <li><a href="#" data-original-title="Twitter" class="rounded-x social_twitter"></a></li>
                            <li><a href="#" data-original-title="Goole Plus" class="rounded-x social_googleplus"></a></li>
                            <li><a href="#" data-original-title="Linkedin" class="rounded-x social_linkedin"></a></li>
                            <li><a href="#" data-original-title="Pinterest" class="rounded-x social_pintrest"></a></li>
                        </ul>
                    </div>
                    <!-- End Social Links -->
                </div>
            </div> 
        </div><!--/copyright-->
    </div>     
    <!--=== End Footer Version 1 ===-->
</div><!--/wrapper-->

<!-- JS Global Compulsory -->           
<script type="text/javascript" src="assets/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="assets/plugins/jquery/jquery-migrate.min.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
<!-- JS Implementing Plugins -->           
<script type="text/javascript" src="assets/plugins/back-to-top.js"></script>
<script type="text/javascript" src="assets/plugins/smoothScroll.js"></script>
<script type="text/javascript" src="assets/plugins/jquery.mixitup.min.js"></script>
<script type="text/javascript" src="assets/plugins/parallax-slider/js/modernizr.js">
</script>

<script type="text/javascript" src="assets/plugins/parallax-slider/js/jquery.cslider.js"></script>
<!-- JS Customization -->
<script type="text/javascript" src="assets/js/custom.js"></script>
<!-- JS Page Level -->           
<script type="text/javascript" src="assets/js/app.js"></script>
<script type="text/javascript" src="assets/js/pages/page_portfolio.js"></script>
<script type="text/javascript" src="assets/js/plugins/style-switcher.js"></script>
<script type="text/javascript" src="assets/js/plugins/parallax-slider.js"></script>


<script type="text/javascript">
    jQuery(document).ready(function() {
        App.init();
        PortfolioPage.init();
        StyleSwitcher.initStyleSwitcher();        
        ParallaxSlider.initParallaxSlider();        
    });
</script>



<!--[if lt IE 9]>
    <script src="assets/plugins/respond.js"></script>
    <script src="assets/plugins/html5shiv.js"></script>
    <script src="assets/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->