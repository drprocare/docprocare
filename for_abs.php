<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  

<!-- Mirrored from htmlstream.com/preview/unify-v1.8/feature_team_blocks.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Jan 2016 17:14:09 GMT -->
<head>
    <title>Drprocare | For ABS</title>    

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">

    <!-- Web Fonts -->
    <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin'>

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">

    <!-- CSS Header and Footer -->
    <link rel="stylesheet" href="assets/css/headers/header-default.css">
    <link rel="stylesheet" href="assets/css/footers/footer-v1.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="assets/plugins/animate.css">
    <link rel="stylesheet" href="assets/plugins/line-icons/line-icons.css">
    <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/plugins/cube-portfolio/cubeportfolio/css/cubeportfolio.min.css">
    <link rel="stylesheet" href="assets/plugins/cube-portfolio/cubeportfolio/custom/custom-cubeportfolio.css">

    <!-- CSS Theme -->    
    <link rel="stylesheet" href="assets/css/theme-colors/default.css" id="style_color">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="assets/css/custom.css">

    <?php include 'head.php'; ?>
</head>	

<body>

<div class="wrapper">
    <!--=== Header ===-->    
    <?php include 'header.php'; ?>
    <!--=== End Header ===-->

  

    <!--=== Team v7 ===-->
    <div class="container-fluid" style="margin-top: 10px;">
        <!-- Team Blcoks -->
        <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 team-arrow-right">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 500px;">
                        <span class="team-v7-name">FOR ABS</span>
                       <!--  <span class="team-v7-position">Co-Founder / CEO</span> -->
                        <p>Steps for Plank Pose:</p>
                         <p>1. Start on your hands and knees in Tabletop Pose, and align your wrists directly under your shoulders </p>
                         <p>2. Come to the balls of your feet as you extend your legs behind you, lifting your knees off the mat (it looks like the top of a push-up) </p>
                         <p>3. Slightly tuck your tailbone to activate muscles in the lower belly</p>
                         <p>4. Engage the muscles in your legs, lifting up through the quads to keep your body strong </p>
                         <p>5. Keep your core strong to prevent hips from sagging down or picking up</p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/yoga/for_abs/abs-1.jpg" alt="" style="height: 500px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

        <!-- Team Blcoks -->
        <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 col-md-push-6 team-arrow-left">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 500px;">
                        <span class="team-v7-name">DOLPHIN POSE</span>                        
                        <!-- <span class="team-v7-position">Co-Founder/ UX Design</span> -->
                        <p>Dolphin Pose works the abdominals while toning the shoulders and lengthening the spine. Holding Dolphin Pose will engage all muscles in your core, both superficial and deep muscles.</p>

                        <p>Steps for Dolphin Pose</p>
                        <p> 1. From Plank Pose drop your forearms to the mat, keeping your wrist in line with your elbow and palm on the earth</p>
                        <p> 2. Keep elbows directly under the shoulders</p>
                        <p> 3. Lift your hips towards the sky, and walk your toes in towards your face – like a Downward </p>
                        <p>4. Facing Dog on your forearms </p>
                        <p>5. For more intensity, lift hips above the shoulders and relax your heels toward the ground</p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-md-pull-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/yoga/for_abs/abs-2.jpg" alt="" style="height: 500px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

         <!-- Team Blcoks -->
        <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 team-arrow-right">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 500px;">
                        <span class="team-v7-name">DOLPHIN POSE WITH LEG LIFT (Catur Svanasana):</span>
                       <!--  <span class="team-v7-position">Co-Founder / CEO</span> -->
                        <p>Dolphin Pose with a leg lift fires up the front body – the muscles that run from just below your sternum to your waist. This Dolphin variation is one of the best exercises for core conditioning</p>

                        <p>Steps for Dolphin Pose With Leg Lift</p>
                        <p> 1. Start in Dolphin Pose</p>
                        <p> 2. Walk your feet in towards your elbows to align your hips over your shoulders (or at least as much as possible!)</p>
                        <p> 3. Lift your head just enough to maintain a neutral neck – don’t look too far forward or back 
                        <p>4. Alternate lifting each leg toward the sky</p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/yoga/for_abs/abs-3.jpg" alt="" style="height: 500px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

        <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 col-md-push-6 team-arrow-left">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name">STAR PLANK (SIDE PLANK VARIATION)</span>                        
                        <span class="team-v7-position">Co-Founder/ UX Design</span>
                        <p>This Side Plank variation will help tone those famous “love handles” or obliques. Your waist will activate as you lift the body, and your abdominals will be working to stabilize the entire body. By lifting the top leg, you will further strengthen your obliques and lower abs.</p>

                        <p>Steps for Star Plank:</p>
                        <p> 1. From Plank Pose, shift your weight onto one hand and transition into Side Plank </p>
                        <p> 2. For less pressure on your wrists, drop down to your forearm for a Side Forearm Plank</p>
                        <p> 3. Stack your top leg on top of bottom, and slowly lift your top leg away from the bottom leg 
                        <p> 4. To modify the pose, simply drop to the bottom knee or keep the legs together</p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-md-pull-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/yoga/for_abs/abs-4.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

         <!-- Team Blcoks -->
        <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 team-arrow-right">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name">UPWARD FACING PLANK POSE</span>
                        <span class="team-v7-position">Co-Founder / CEO</span>
                        <p>Upward Facing Plank Pose core strength while challenging and improving balance. This pose is a great way to counterbalance the motion of sitting at a desk for many hours of the day. In this Plank variation we open up the front body while strengthening the muscles along the spine. Steps for</p>

                        <p>Steps for Upward Facing Plank Pose</p>
                        <p> 1. Begin seated on your mat with legs extended long in front of you </p>
                        <p> 2. Bring your hands behind you, palms flat on the mat under your shoulders with your fingers facing your body</p>
                        <p> 3. Lift hips upwards and allow feet to fall towards the floor, pressing flat on the ground if possible</p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/yoga/for_abs/abs-5.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

         <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 col-md-push-6 team-arrow-left">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name">HALF BOW-HALF LOCUST</span>                        
                        <span class="team-v7-position">Co-Founder/ UX Design</span>
                        <p>This fun variation of traditional Bow and Locust Pose strengthens the muscles that surround the spine. It is a great counterpose to the others that target the front core muscles.</p>

                        <p>Steps for Half Bow-Half Locust</p>
                        <p> 1. Start lying flat on your belly and reach one hand back</p>
                        <p> 2. Grab a hold of the ankle on that same side </p>
                         <p>3. Extend the opposite arm straight out in front of you</p> 
                         <p>4. As you lift your chest off the ground, press your foot into your hand and lift up into a Half Bow Pose</p>
                         <p> 5. Simultaneously lift the opposite leg and arm into a Half Locust Pose</p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-md-pull-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/yoga/for_abs/abs-6.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

        <!-- Team Blcoks -->
        <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 team-arrow-right">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name">TWISTING BOAT POSE</span>
                        <span class="team-v7-position">Co-Founder / CEO</span>
                        <p>Boat pose engages the deep abdominal muscles and tests the strength and stamina of every muscle from your hips to your shoulders. By incorporating the twist you will also fire up the side body-both the internal and external obliques..</p>

                        <p>Steps for Twisting Boat Pose</p>
                        <p> 1. From Boat Pose keep your shins parallel to the floor and knees bent (for a more advanced variations you can straighten the legs)</p> 
                        <p>2. Hold Boat Pose for one breath with arms extended straight out in front of you</p>
                        <p> 3. Twist to one side, opening your arms wide </p>
                        <p>4. Come back to center and repeat on opposite side</p>

                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/yoga/for_abs/abs-7.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

         <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 col-md-push-6 team-arrow-left">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name">FLOATING TRIANGLE</span>                        
                        <span class="team-v7-position">Co-Founder/ UX Design</span>
                        <p>Triangle Pose is taught in almost every yoga class, however we rarely do the floating variation. By floating the arms overhead we begin to lengthen and strengthen the obliques, abdominals, and back.</p>

                        <p>Steps for Floating Triangle Pose</p>
                        <p> 1. Begin in Triangle Pose and extend the bottom arm straight out in front of you </p>
                        <p>2. Bring the top arm over your head, with your bicep close to your ear </p>
                        <p>3. Imagine holding a giant beach ball over your head</p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-md-pull-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/yoga/for_abs/abs-8.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

        <!-- Team Blcoks -->
        <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 team-arrow-right">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name">CHAIR POSE</span>
                        <span class="team-v7-position">Co-Founder / CEO</span>
                        <p>Looks can be deceiving when it comes to Chair Pose. However, Chair Pose requires a great deal of stability through the core in addition to strength in the legs and flexibility in the shoulder. All of your core muscles will be engaged as you hold the pose and practicing this pose regularly can help improve your posture over time.</p>

                        <p>How to Practice Chair Pose:</p>
                        <p> 1. From Mountain Pose, bend your knees and lower your thighs to be parallel with the floor </p>
                        <p>2. Sit your hips straight back as if sitting into a chair</p>   <p>3. Keep arms extended overhead and torso long as you reach your tailbone down toward the floor and crown of head up to the sky</p> <p>4. Keep your core engaged (imagine drawing your belly button in towards your spine)</p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/yoga/for_abs/abs-9.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

         <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 col-md-push-6 team-arrow-left">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name">WARRIOR III</span>                        
                        <span class="team-v7-position">Co-Founder/ UX Design</span>
                        <p>Although it may most often be thought of as a standing balance pose, Warrior III builds strength and length in all parts of the core. From the front abdomen to the muscles around the spine, you will be engaging your entire core to hold your balance while keeping your body aligned.Steps for</p>

                        <p>Steps for Warrior III </p>
                        <p>1. From Mountain Pose extend your fingertips above your head</p><p>2. Draw one knee towards your chest</p>
                        <p> 3. Begin to kick your lifted foot behind you </p>
                        <p>4. Reach your hands forward in front of you </p>
                        <p>5. Imagine making a capital “T” shape with your body; your torso and extended leg should be parallel to the floor</p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-md-pull-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/yoga/for_abs/abs-10.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->
    </div>
    <!--=== End Team v7 ===-->

    

    <!--=== Footer Version 1 ===-->
   <?php include'footer.php';  ?>
    <!--=== End Footer Version 1 ===-->
</div>




</body>

<!-- Mirrored from htmlstream.com/preview/unify-v1.8/feature_team_blocks.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Jan 2016 17:15:23 GMT -->
</html>