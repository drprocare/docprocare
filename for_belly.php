<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  

<!-- Mirrored from htmlstream.com/preview/unify-v1.8/feature_team_blocks.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Jan 2016 17:14:09 GMT -->
<head>
    <title>Drprocare | For Belly</title>   

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">

    <!-- Web Fonts -->
    <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin'>

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">

    <!-- CSS Header and Footer -->
    <link rel="stylesheet" href="assets/css/headers/header-default.css">
    <link rel="stylesheet" href="assets/css/footers/footer-v1.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="assets/plugins/animate.css">
    <link rel="stylesheet" href="assets/plugins/line-icons/line-icons.css">
    <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/plugins/cube-portfolio/cubeportfolio/css/cubeportfolio.min.css">
    <link rel="stylesheet" href="assets/plugins/cube-portfolio/cubeportfolio/custom/custom-cubeportfolio.css">

    <!-- CSS Theme -->    
    <link rel="stylesheet" href="assets/css/theme-colors/default.css" id="style_color">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="assets/css/custom.css">

     <?php include 'head.php'; ?>
</head> 

<body>

<div class="wrapper">
    <!--=== Header ===-->    
    <?php include 'header.php'; ?>
    <!--=== End Header ===-->

  

    <!--=== Team v7 ===-->
    <div class="container-fluid" style="margin-top: 10px;">
        <!-- Team Blcoks -->
        <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 team-arrow-right">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name">FOR BELLY</span><br>

                        <span class="team-v7-name">Cobra Posture (Bhujang asana)</span>
                       <!--  <span class="team-v7-position">Co-Founder / CEO</span> -->
                        <p>This pose can strengthen the ab muscles to reduce belly fat. It will also help to strengthen the upper body by creating a strong and flexible spine.</p>
                         <p>To perform this pose: 1. Lie on your stomach with your legs stretched out and your palms positioned underneath your shoulders. 2. Your chin and your toes should be touching the floor. 3. Inhale slowly and raise your chest upward while bending backward. 4. Depending on your strength, hold this pose for 15-30 seconds. 5. Exhale slowly and bring your body back to the original position. 6. Repeat 5 times with a rest time of 15 seconds in between each pose. </p>
                         <p>Notes : If you are pregnant, have an ulcer, hernia or back injury, do not attempt this pose.</p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 team-v7-img">
                <img class=for_abs"img-responsive full-width equal-height-column" src="assets/img/yoga/for_belly/belly-1.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

        <!-- Team Blcoks -->
        <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 col-md-push-6 team-arrow-left">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name">Bow Posture (Dhanurasana)</span>                        
                        <!-- <span class="team-v7-position">Co-Founder/ UX Design</span> -->
                        <p>This pose will strengthen your core and tighten your abs to help reduce belly fat. Rocking back and forth in this pose will stimulate the digestive system and fight constipation, while allowing the body to stretch.</p>

                        <p>To perform this pose: 1. Lie on your stomach with your legs stretched out and your arms at each side of your body. 2. Bend at the knees and reach your arms back to your ankles or feet and hold. 3. Inhale and lift your head, then bend it backward while lifting your legs as high as possible. 4. Try to hold this pose for 15-30 seconds and keep breathing normally. 5. Begin to exhale and move back to the original position with your legs stretched out and your arms at your sides. 6. Repeat at least 5 times with a rest time of 15 seconds in between each pose.</p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-md-pull-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/yoga/for_belly/belly-2.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

         <!-- Team Blcoks -->
        <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 team-arrow-right">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name">Pontoon Posture (Naukasana)</span>
                       <!--  <span class="team-v7-position">Co-Founder / CEO</span> -->
                        <p>This pose will help to attack belly fat near the waist, while strengthening the muscles in the legs and back.</p>

                        <p>To perform this pose: 1. Lie on your back with your legs stretched out but together, and your arms at your sides. 2. Inhale and begin to raise your legs, keeping them straight. 3. Stretch your feet and toes and raise your legs as high as you can without bending your knees. 4. Lift your arms to reach your toes and create a 45-degree angle with your body. 5. Keep breathing normally and hold this pose for 15 seconds. 6. Release and exhale. 7. Repeat this pose 5 times with a rest time of 15 seconds in between each pose</p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/yoga/for_belly/belly-3.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

        <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 col-md-push-6 team-arrow-left">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name">Kumbhakasana</span>                        
                        <span class="team-v7-position">Co-Founder/ UX Design</span>
                        <p>This is an easy pose to perform. It burns belly fat while toning and strengthening the shoulders, arms, back, thighs and butt.</p>

                        <p>To perform this pose: 1. Start with your hands and knees underneath your arms and shoulders. 2. Tuck your toes under and step your feet back to extend your legs behind your body. 3. Inhale while looking just ahead of your palms, so your neck and spine are aligned. 4. Tighten your ab muscles and hold.5. Your body should form a straight line. Make sure your hands are flat on the ground and your fingers are spread apart. 6. Hold for 15-30 seconds or as long as possible. 7. Exhale and drop to your knees. 8. Repeat this pose five times with a rest time of atleast 15 seconds in between each pose.</p>

                        <p>Notes : If you have high blood pressure or any type of shoulder or back injury, do not attempt this pose.</p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-md-pull-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/yoga/for_belly/belly-4.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

         <!-- Team Blcoks -->
        <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 team-arrow-right">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name">Wind Easing Posture (Pavanamukthasana)</span>
                        <span class="team-v7-position">Co-Founder / CEO</span>
                        <p>This pose can help to relieve lower back pain while toning the abs, thighs and hips. It is also believed to balance pH levels in the body to enhance the metabolism and promote stomach health.</p>

                        <p>To perform this pose: 1. Lie flat on your back with your legs stretched out and your arms at your sides. 2. Your feet should be stretched out with your heels touching each other. 3. Exhale and bend at the knees while gradually bringing them toward the chest. 4. Apply pressure to the abdominals using your thighs. 5. Clasp your hands underneath your thighs to hold your knees in place. 6. Breathe deeply while holding this pose for 60-90 seconds. 7. Exhale while releasing your knees and bringing your hands to the sides of your body. 8. Repeat this pose 5 times with at least 15 seconds of rest time in between each pose.</p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/yoga/for_belly/belly-5.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

         
    </div>
    <!--=== End Team v7 ===-->

    

    <!--=== Footer Version 1 ===-->
   <?php include'footer.php';  ?>
    <!--=== End Footer Version 1 ===-->
</div>




</body>

<!-- Mirrored from htmlstream.com/preview/unify-v1.8/feature_team_blocks.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Jan 2016 17:15:23 GMT -->
</html>