<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  

<!-- Mirrored from htmlstream.com/preview/unify-v1.8/feature_team_blocks.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Jan 2016 17:14:09 GMT -->
<head>
    <title>Drprocare | For Brain</title>     

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">

    <!-- Web Fonts -->
    <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin'>

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">

    <!-- CSS Header and Footer -->
    <link rel="stylesheet" href="assets/css/headers/header-default.css">
    <link rel="stylesheet" href="assets/css/footers/footer-v1.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="assets/plugins/animate.css">
    <link rel="stylesheet" href="assets/plugins/line-icons/line-icons.css">
    <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/plugins/cube-portfolio/cubeportfolio/css/cubeportfolio.min.css">
    <link rel="stylesheet" href="assets/plugins/cube-portfolio/cubeportfolio/custom/custom-cubeportfolio.css">

    <!-- CSS Theme -->    
    <link rel="stylesheet" href="assets/css/theme-colors/default.css" id="style_color">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="assets/css/custom.css">

     <?php include 'head.php'; ?>
</head> 

<body>

<div class="wrapper">
    <!--=== Header ===-->    
    <?php include 'header.php'; ?>
    <!--=== End Header ===-->

  

    <!--=== Team v7 ===-->
    <div class="container-fluid" style="margin-top: 10px;">
        <!-- Team Blcoks -->
        <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 team-arrow-right">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name">For Brain</span><br>
                        <span class="team-v7-name">Bhramari pranayama (Bee Breath)</span>
                       <!--  <span class="team-v7-position">Co-Founder / CEO</span> -->
                        <p>Steps:1. Sit up straight in a quiet, well-ventilated corner with your eyes closed. Keep a gentle smile on your face.2. Keep your eyes closed for some time. Observe the sensations in the body and the quietness within.3. Place your index fingers on your ears. There is a cartilage between your cheek and ear. Place your index fingers on the cartilage.4. Take a deep breath in and as you breathe out, gently press the cartilage. You can keep the cartilage pressed or press it in and out with your fingers while making a loud humming sound like a bee.5. You can also make a low-pitched sound but it is a good idea to make a high-pitched one for better results.6. Breathe in again and continue the same pattern 3-4 times.</p>

                        <p>Advantages:1:Helps calm the mind,2 : Releases negative emotions like anger, agitation, frustration, and anxiety 3: Improves concentration and memory 4:Builds confidence</p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/yoga/for_brain/brain-1.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

        <!-- Team Blcoks -->
        <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 col-md-push-6 team-arrow-left">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name">Paschimottanasana (Seated Forward Bend):</span>                        
                        <!-- <span class="team-v7-position">Co-Founder/ UX Design</span> -->
                        <p>Steps:
1. Sit up with the legs stretched out straight in front of you, keeping the spine erect and toes flexed towards you.
2. Breathing in, raise both arms above your head and stretch up.
3. Breathing out, bend forward from the hip joints, chin moving toward the toes. Keep the spine erect focusing on moving forwards towards the toes, rather than down towards the knees.
4. Place your hands on your legs, wherever they reach, without forcing. If you can, take hold of your toes and pull on them to help you go forward.
5. Breathing in, lift your head slightly and lengthen your spine.
6. Breathing out, gently move the navel towards the knees.
7. Repeat this movement two or three times.
8. Drop your head down and breathe deeply for 20-60 seconds.
9. Stretch the arms out in front of you.
10. Breathing in, with the strength of your arms, come back up to the sitting position.
11. Breathe out and lower the arms.</p>

                        <p>Advantages:
 Stretches the spine, helps relieve stress
 Relaxes the mind by removing negative emotions like irritability and anger</p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-md-pull-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/yoga/for_brain/brain-2.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

         <!-- Team Blcoks -->
        <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 team-arrow-right">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name">Setu Bandhasana(Bridge pose)</span>
                       <!--  <span class="team-v7-position">Co-Founder / CEO</span> -->
                        <p>Steps:
1. To begin, lie on your back.
2. Fold your knees and keep your feet hip distance apart on the floor, 10-12 inches from your pelvis, with knees and ankles in a straight line.
3. Keep your arms beside your body, palms facing down.
4. Inhaling, slowly lift your lower back, middle back and upper back off the floor; gently roll in the shoulders; touch the chest to the chin without bringing the chin down, supporting your weight with your shoulders, arms and feet. Feel your bottom firm up in this pose. Both the thighs are parallel to each other and to the floor.
5. If you wish, you could interlace the fingers and push the hands on the floor to lift the torso a little more up, or you could support your back with your palms.
6. Keep breathing easily.
7. Hold the posture for a minute or two and exhale as you gently release the pose.</p>

                        <p>Advantages:
 Strengthens and stretches the neck and spine, relaxing tight muscles and improving blood circulation to the brain
 Calms the brain and nervous system, thereby reducing anxiety, stress, and depression</p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/yoga/for_brain/brain-4.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

        <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 col-md-push-6 team-arrow-left">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name">Halasana (Plow Pose):</span>                        
                        <span class="team-v7-position">Co-Founder/ UX Design</span>
                        <p>1. Lie flat on the floor keeping the arms by the side of the body. Palms facing downward and legs together. Relax the body taking a few deep and slow breaths.
2. Using the strength of your abdominal muscles, slowly lift the legs off the ground until they are perpendicular to the floor. Keep the legs straight and together.
3. Gently press your arms against the floor and raise your buttocks. Continue to roll the spine till your big toes reach the ground over your head. Don’t force your feet to touch the ground. Keep the legs straight.
4. Try to keep the spine as straight as possible. Ideally, the spine is perpendicular to the floor in the final position. A beginner can take the support of arms by placing the hands behind the ribcage to support the back.
5. Bring your arms closer and interlock the fingers of both of your hands. If this feels tough, simply keep the arms closer and join the thumps.
6. In the final position, the chin is tucked in the center of collar bones. However, it takes time to achieve this position. Don’t force yourself into it as that may strain the muscles of your neck.7. Stretch the legs and arms in the opposite direction. Hold the pose for around 15 seconds to a minute depending on how long you feel comfortable. Take slow and deep breaths.
8. To release the pose, gently lower the spine and bring the legs in vertical position. Slowly lower the legs to the ground. Relax the whole body.</p>

                        <p>Advantages:
 Helps improve blood flow to the brain and calm the nervous system
 Stretches the back and neck, reducing stress and fatigue</p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-md-pull-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/yoga/for_brain/brain-3.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

         <!-- Team Blcoks -->
        <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 team-arrow-right">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name">Sarvangasana (Shoulder Stand):</span>
                        <span class="team-v7-position">Co-Founder / CEO</span>
                        <p>Steps:
1. Lie on your back with hands by your side.
2. With one movement, lift your legs, buttocks and back so that you come up high on your shoulders. Support your back with the hands.
3. Move your elbows closer towards each other, and move your hands along your back, creeping up towards the shoulder blades. Keep straightening the legs and spine by pressing the elbows down to the floor and hands into the back. Your weight should be supported on your shoulders and upper arms and not on your head and neck.4. Keep the legs firm. Lift your heels higher as though you are putting a footprint on the ceiling. Bring the big toes straight over the nose. Now point the toes up. Pay attention to your neck. Do not press the neck into the floor. Instead keep the neck strong with a feeling of tightening the neck muscles slightly. Press your sternum toward the chin. If you feel any strain in the neck, come out of the posture.
5. Keep breathing deeply and stay in the posture for 30-60 seconds.
6. To come out of the posture, lower the knees to forehead. Bring your hands to the floor, palms facing down. Without lifting the head slowly bring your spine down, vertebra by vertebra, completely to the floor. Lower the legs to the floor. Relax for a minimum of 60 seconds.</p>

                        <p>Advantages:
 Regulates and normalizes the functioning of thyroid and parathyroid glands, thus improvingthe brain’s functioning
 Nourishes the brain as more blood reaches the pineal and hypothalamus glands
 Helps improve all cognitive functions</p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/yoga/for_brain/brain-5.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

         <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 col-md-push-6 team-arrow-left">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name">How to do super brain yoga?</span>                        
                        <span class="team-v7-position">Co-Founder/ UX Design</span>
                        <p>1. Stand tall and straight, with your arms on the side.
2. Lift your left arm and hold your right earlobe with your thumb and index finger. Your thumb should be in front.
3. Lift your right arm and hold your left earlobe. Your right arm should be over your left arm.
4. Inhale deeply and squat down slowly to a sitting position.
5. Stay in this position for 2-3 seconds.
6. Gently exhale as you rise up again. This completes one cycle.
7. You may repeat this cycle about 15 times every day.</p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-md-pull-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/yoga/for_brain/brain-6.png" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

         <!-- Team Blcoks -->
        <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 team-arrow-right">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name">Benefits of super brain yoga:</span>
                        <span class="team-v7-position">Co-Founder / CEO</span>
                        <p>Synchronizing left and right side of the brain
 Distributing energy levels and increasing sense of calmness
 Stimulating thinking capacity
 Increasing mental energy
 Making you more creative
 Developing cognitive powers
 Improving focus, concentration and memory power
 Boosting decision-making skills
 Relieving stress or behavioral problems
 Making you more psychologically balanced</p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/yoga/for_brain/brain-7.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->
        
    </div>
    <!--=== End Team v7 ===-->

    

    <!--=== Footer Version 1 ===-->
   <?php include'footer.php';  ?>
    <!--=== End Footer Version 1 ===-->
</div>




</body>

<!-- Mirrored from htmlstream.com/preview/unify-v1.8/feature_team_blocks.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Jan 2016 17:15:23 GMT -->
</html>