<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  

<!-- Mirrored from htmlstream.com/preview/unify-v1.8/page_home7.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Jan 2016 17:09:10 GMT -->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5cc8348bd07d7e0c639143a5/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<head>
    <title>Drprocare | Home</title>    

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
   

<?php include 'head.php';  ?>
   
</head> 

<body>

<div class="wrapper">
    <!--=== Header ===-->    
    <?php include 'header.php'; ?>
    <!--=== End Header ===-->

    <!--=== Slider ===-->
    <div class="slider-inner">
        <div id="da-slider" class="da-slider">
            <div class="da-slide">



                <h2><i>Doctors, Hospitals and Pharmacy willing &amp;  <br/>to promote and advertise</i> <br /> <i>their respective</i> <br /> <i>organization can collaborate with us</i></h2>
                
              <!--   <div class="da-img"><img class="img-responsive" src="assets/plugins/parallax-slider/img/1.png" alt=""></div> -->
            </div>
            <div class="da-slide">
                

                <h2><i>Book appointment </i> <br /> <i>online with ease</i> <br /> <i>Consult a specialist anytime and anywhere</i></h2>
                <p><i>Feel healthy, Feel Good</i> <br /></p>
                
            </div>
         <!--    <div class="da-slide">
                <h2><i>USING BEST WEB</i> <br /> <i>SOLUTIONS WITH</i> <br /> <i>HTML5/CSS3</i></h2>
                <p><i>Lorem ipsum dolor amet</i> <br /> <i>tempor incididunt ut</i> <br /> <i>veniam omnis </i></p>
                <div class="da-img"><img src="assets/plugins/parallax-slider/img/html5andcss3.png" alt="image01" /></div>
            </div> -->
            <div class="da-arrows">
                <span class="da-arrows-prev"></span>
                <span class="da-arrows-next"></span>        
            </div>
        </div>
    </div><!--/slider-->
    <!--=== End Slider ===-->

    <!--=== Content Part ===-->
    <div class="container content">
        <!-- Begin Service Block -->
        <div class="row margin-bottom-40">
            <div class="col-md-4 col-sm-6">
                <div class="service-block service-block-sea service-or">
                    <div class="service-bg"></div>                
                    <i class="icon-custom icon-color-light rounded-x fa fa-lightbulb-o"></i>
                    <h2 class="heading-md">call Today +91-7622884442 Ask for Doctor</h2>
                    <!-- <p>Donec id elit non mi porta gravida at eget metus id elit mi egetine. Fusce dapibus</p> -->                        
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="service-block service-block-red service-or">
                    <div class="service-bg"></div>                
                    <i class="icon-custom icon-color-light rounded-x icon-line icon-fire"></i>
                    <h2 class="heading-md">Open Hours Mon-sun(08:00-23:00 hrs)</h2>
                    <!-- <p>Donec id elit non mi porta gravida at eget metus id elit mi egetine usce dapibus elit nondapibus</p> -->
                </div>
            </div>
            <div class="col-md-4 col-sm-13">
                <div class="service-block service-block-blue service-or">
                    <div class="service-bg"></div>                
                    <i class="icon-custom icon-color-light rounded-x icon-line icon-rocket"></i>
                    <h2 class="heading-md">For an Appointment</br> Book Now</h2>
                   <!--  <p>Donec id elit non mi porta gravida at eget metus id elit mi egetine. Fusce dapibus</p> -->
                </div>
            </div>
        </div>
        <!-- End Begin Service Block -->
    <!-- About Me Block -->
    
    <div class="">
        <div class="row about-me">
            
            </div>
        </div>
    </div>
    <!-- End About Me Block -->

    <!--=== Parallax About Block ===-->
    <div class="parallax-bg parallaxBg1">            
        <div class="container content parallax-about">
            <div class="title-box-v2">
                <h2>Why <span class="color-green">Dr.procare?</span></h2>
                
                
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="banner-info dark margin-bottom-10">
                        <i class="rounded-x icon-bell"></i>
                        <div class="overflow-h">


                            <p>Dr.procare is focused on rejuvenating doctor-patient relationship. We provide a simple and convenient communication tool through which doctors can unchain themselves from their medical offices and still take great care of patients. With an eye to the future, we are committed to innovating our way to improving the world's health.</p>
                            <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tincidunt sit amet dui auctor pellentesque. Nulla ut posuere purus.</p> -->
                        </div>
                    </div>
                    <div class="banner-info dark margin-bottom-10">
                        <i class="rounded-x fa fa-magic"></i>
                        <div class="overflow-h">
                            <p>Dr.procare is the place where you can find a healing touch. It connects doctor and patient with everything they need to take good care of themselves and their family - assessing health issues, finding the right doctor, obtaining medicines, booking diagnostic tests, storing health records, learning new ways to live healthier or booking online doctor’s appointment.</p>
                            <!-- <h3>Our vision</h3> -->
                            <!-- <p>Phasellus vitae rhoncus ipsum. Aliquam ultricies, velit sit amet scelerisque tincidunt, dolor neque consequat est, a dictum felis metus eget nulla.</p> -->
                        </div>
                    </div>
                    <div class="banner-info dark margin-bottom-10">
                        <i class="rounded-x fa fa-thumbs-o-up"></i>
                        <div class="overflow-h">
                            <p>We intend to signify the intrinsic motivation in each of the field to strive for excellence. Every time we do our best work, not for want of rewards or recognitions but because the work expect for itself.</p>
                            <!-- <h3>We are good at ...</h3> -->
                          <!--   <p>Nunc ac ligula ut diam rutrum porttitor. Integer et neque at lacus placerat pretium eu ac dui. Class aptent taciti sociosqu ad litora torquent per conubia nostra.</p> -->
                        </div>
                    </div>
                    <div class="margin-bottom-20"></div>
                </div>
                <div class="col-md-6">
                    <img class="img-responsive" src="assets/img/mockup/6.png" alt="">
                </div>
            </div>    
        </div><!--/container--> 
    </div>  </br>    
    <!--=== End Parallax About Block ===-->

        <!--=== Team Section with Parallax ===-->
    <!-- 
        Requires Parallax plugin and js file should be linked
     -->
    
    <div class="parallax-team parallaxBg">
        <div class="container content-md">
            
            <div class="row">
                
                <!-- Team v2 -->
                  <div class="title-box-v2">
                <h1>Our <span class="color-green">Specialist</span></h1>
                
            </div>
                <div class="col-md-3 col-sm-6">

                    <div class="team-v2 no-margin-bottom">

                        <img class="img-responsive" src="assets/img/team/ab1.jpg" alt="">
                        <div class="inner-team">
                            <h3>Micheal</h3>
                            <small class="color-green">CEO, Chief Officer</small>
                            <!-- <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, justo sit amet risus etiam porta sem...</p> -->
                            <hr>    
                            <ul class="list-inline team-social">
                                <li>
                                    <a data-placement="top" data-toggle="tooltip" class="fb tooltips" data-original-title="Facebook" href="#">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a data-placement="top" data-toggle="tooltip" class="tw tooltips" data-original-title="Twitter" href="#">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a data-placement="top" data-toggle="tooltip" class="gp tooltips" data-original-title="Google plus" href="#">
                                        <i class="fa fa-google-plus"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- End Team v2 -->

                <!-- Team v2 -->
                <div class="col-md-3 col-sm-6">
                    <div class="team-v2 no-margin-bottom">
                        <img class="img-responsive" src="assets/img/team/ab2.jpg" alt="">
                        <div class="inner-team">
                            <h3>Andy</h3>
                            <small class="color-green">Project Manager</small>
                            <!-- <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, justo sit amet risus etiam porta sem...</p> -->
                            <hr>    
                            <ul class="list-inline team-social">
                                <li><a data-placement="top" data-toggle="tooltip" class="fb tooltips" data-original-title="Facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a data-placement="top" data-toggle="tooltip" class="tw tooltips" data-original-title="Twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a data-placement="top" data-toggle="tooltip" class="gp tooltips" data-original-title="Google plus" href="#"><i class="fa fa-google-plus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- End Team v2 -->

                <!-- Team v2 -->
                <div class="col-md-3 col-sm-6">
                    <div class="team-v2 no-margin-bottom">
                        <img class="img-responsive" src="assets/img/team/ab3.jpg" alt="">
                        <div class="inner-team">
                            <h3>Shawn</h3>
                            <small class="color-green">VP of Operations</small>
                          <!--   <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, justo sit amet risus etiam porta sem...</p> -->
                            <hr>    
                            <ul class="list-inline team-social">
                                <li>
                                    <a data-placement="top" data-toggle="tooltip" class="fb tooltips" data-original-title="Facebook" href="#">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a data-placement="top" data-toggle="tooltip" class="tw tooltips" data-original-title="Twitter" href="#">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a data-placement="top" data-toggle="tooltip" class="gp tooltips" data-original-title="Google plus" href="#">
                                        <i class="fa fa-google-plus"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- End Team v2 -->

                <!-- Team v2 -->
                <div class="col-md-3 col-sm-6">
                    <div class="team-v2 no-margin-bottom">
                        <img class="img-responsive" src="assets/img/team/ab4.jpg" alt="">
                        <div class="inner-team">
                            <h3>Amy</h3>
                            <small class="color-green">Director, R &amp; D Talent</small>
                            <!-- <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, justo sit amet risus etiam porta sem...</p> -->
                            <hr>    
                            <ul class="list-inline team-social">
                                <li>
                                    <a data-placement="top" data-toggle="tooltip" class="fb tooltips" data-original-title="Facebook" href="#">
                                        <i class="fa fa-facebook"></i></a>
                                </li>
                                <li>
                                    <a data-placement="top" data-toggle="tooltip" class="tw tooltips" data-original-title="Twitter" href="#">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a data-placement="top" data-toggle="tooltip" class="gp tooltips" data-original-title="Google plus" href="#">
                                        <i class="fa fa-google-plus"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- End Team v2 -->
            </div>            
        </div>
    </div>
    <!--=== End Team Section with Parallax ===-->    
    <!-- End Content Part -->

    <!--=== Service Blocks ===-->
    <div class="container content-sm">
        <div class="text-center margin-bottom-50">
            <h2 class="title-v2 title-center">OUR SERVICES</h2>
            <!-- <p class="space-lg-hor">If you are going to use a <span class="color-green">passage of Lorem Ipsum</span>, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making <span class="color-green">this the first</span> true generator on the Internet.</p> -->
        </div>

        <div class="row content-boxes-v4 content-boxes-v4-sm margin-bottom-30">
            <div class="col-md-4 md-margin-bottom-40">
                <i class="pull-left icon-directions"></i>
                <div class="content-boxes-in-v4">
                    <h2>CARDIOLOGY</h2>
                    <!-- <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p> -->
                    <a href="">Learn More</a>
                </div>
            </div>
            <div class="col-md-4 md-margin-bottom-40">
                <i class="pull-left icon-handbag"></i>
                <div class="content-boxes-in-v4">
                    <h2>RADIOLOGY</h2>
                   <!--  <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p> -->
                    <a href="">Learn More</a>
                </div>
            </div>
            <div class="col-md-4">
                <i class="pull-left icon-chemistry"></i>
                <div class="content-boxes-in-v4">
                    <h2>DENTAL</h2>
                    <!-- <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p> -->
                    <a href="">Learn More</a>
                </div>
            </div>
        </div>        

        <hr>

        <div class="row content-boxes-v4 content-boxes-v4-sm margin-bottom-30">
            <div class="col-md-4 md-margin-bottom-40">
                <i class="pull-left icon-info margin-right-10"></i>
                <div class="content-boxes-in-v4">
                    <h2>PEDIATRICS</h2>
                    <!-- <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p> -->
                    <a href="">Learn More</a>
                </div>
            </div>
            <div class="col-md-4 md-margin-bottom-40">
                <i class="pull-left icon-social-youtube margin-right-10"></i>
                <div class="content-boxes-in-v4">
                    <h2>DERMITOLOGY</h2>
                    <!-- <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p> -->
                    <a href="">Learn More</a>
                </div>
            </div>
            <div class="col-md-4">
                <i class="pull-left icon-fire margin-right-10"></i>
                <div class="content-boxes-in-v4">
                    <h2>DERMITOLOGY</h2>
                   <!--  <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p> -->
                    <a href="">Learn More</a>
                </div>
            </div>
        </div>        

        <hr>
        
    </div>
    <!--=== End Service Blocks ===-->

    <!-- Testimonials Carousel -->
    <div class="testimonials-bs bg-image-v2 parallaxBg1">
        <div class="container">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="item active">
                        <div class="headline-center-v2 headline-center-v2-dark">
                            <h2>Testimonial</h2>
                            <span class="bordered-icon"><i class="fa fa-th-large"></i></span>
                            
                        <div class="flat-testimonials-in md-margin-bottom-50">
                        <img class="rounded-x img-responsive" src="assets/img/team/img1-sm.jpg" alt="" style="display:inline-block;">
                        <h3>Anthony Connor</h3>
                        <span class="color-green">Software Developer</span>
                        <!-- <p>Proin et augue vel nisi rhoncus tincidunt. Cras venenatis, magna id sem ipsum mi interduml</p> -->
                        </div>    
                
                           
                        </div>
                    </div>
                    <div class="item">
                        <div class="headline-center-v2 headline-center-v2-dark">
                            <h2>Testimonial</h2>
                            <span class="bordered-icon"><i class="fa fa-th-large"></i></span>
                            <div class="flat-testimonials-in md-margin-bottom-50">
                                <img class="rounded-x img-responsive" src="assets/img/team/img2-sm.jpg" alt="" style="display:inline-block;">
                                <h3>Anthony Connor</h3>
                                <span class="color-green">Software Developer</span>
                                <!-- <p>Proin et augue vel nisi rhoncus tincidunt. Cras venenatis, magna id sem ipsum mi interduml</p> -->
                            </div>
                        </div>
                    </div>
                     <div class="item">
                        <div class="headline-center-v2 headline-center-v2-dark">
                            <h2>Testimonial</h2>
                            <span class="bordered-icon"><i class="fa fa-th-large"></i></span>
                           <div class="flat-testimonials-in md-margin-bottom-50">
                                <img class="rounded-x img-responsive" src="assets/img/team/img3-sm.jpg" alt="" style="display:inline-block;">
                                <h3>Anthony Connor</h3>
                                <span class="color-green">Software Developer</span>
                                <!-- <p>Proin et augue vel nisi rhoncus tincidunt. Cras venenatis, magna id sem ipsum mi interduml</p> -->
                            </div>
                        </div>
                    </div>
                </div>

                <div class="carousel-arrow">
                    <a class="right carousel-control-v2" href="#carousel-example-generic" data-slide="next">
                        <i class="rounded-x fa fa-angle-left"></i>
                    </a>
                    <a class="left carousel-control-v2" href="#carousel-example-generic" data-slide="prev">
                        <i class="rounded-x fa fa-angle-right"></i>
                    </a>
                </div>
            </div>           
        </div>
    </div>
    <!-- End Testimonials Carousel -->

     <!-- Portfolio Sorting Blocks -->
         <div class="title-box-v2">
                <h1  style="margin-top: 30px;">Our <span class="color-green">Deparments</span></h1>
                
        </div>
        <div class="sorting-block">
            <ul class="sorting-nav sorting-nav-v1 text-center">
                <li class="filter" data-filter="all">Cardiology</li>
                <li class="filter" data-filter="category_1">Neurology</li>
                <li class="filter" data-filter="category_2">Dental</li>
                <li class="filter" data-filter="category_3">Dermitology</li>
                <li class="filter" data-filter="category_3 category_1">Radiology</li>
            </ul>

            <ul class="row sorting-grid">
                <li class="col-md-3 col-sm-6 col-xs-12 mix category_1" data-cat="1">
                    <a href="">
                        <img class="img-responsive" src="assets/img/main/img11.jpg" alt="">
                        <div class="sorting-cover">
                            <span>Happy New Year</span>
                            <!-- <p>Anim pariatur cliche reprehenderit</p> -->
                        </div>
                    </a>
                </li>
                <li class="col-md-3 col-sm-6 col-xs-12 mix category_3" data-cat="3">
                    <a href="">
                        <img class="img-responsive" src="assets/img/main/img12.jpg" alt="">
                        <div class="sorting-cover">
                            <span>Happy New Year</span>
                            <!-- <p>Anim pariatur cliche reprehenderit</p> -->
                        </div>
                    </a>
                </li>
                <li class="col-md-3 col-sm-6 col-xs-12 mix category_2" data-cat="2">
                    <a href="">
                        <img class="img-responsive" src="assets/img/main/img13.jpg" alt="">
                        <div class="sorting-cover">
                            <span>Happy New Year</span>
                            <!-- <p>Anim pariatur cliche reprehenderit</p> -->
                        </div>
                    </a>
                </li>
                <li class="col-md-3 col-sm-6 col-xs-12 mix category_3" data-cat="3">
                    <a href="">
                        <img class="img-responsive" src="assets/img/main/img3.jpg" alt="">
                        <div class="sorting-cover">
                            <span>Happy New Year</span>
                            <!-- <p>Anim pariatur cliche reprehenderit</p> -->
                        </div>
                    </a>
                </li>
                <li class="col-md-3 col-sm-6 col-xs-12 mix category_2" data-cat="2">
                    <a href="">
                        <img class="img-responsive" src="assets/img/main/img2.jpg" alt="">
                        <div class="sorting-cover">
                            <span>Happy New Year</span>
                            <!-- <p>Anim pariatur cliche reprehenderit</p> -->
                        </div>
                    </a>
                </li>
                <li class="col-md-3 col-sm-6 col-xs-12 mix category_1" data-cat="1">
                    <a href="">
                        <img class="img-responsive" src="assets/img/main/img6.jpg" alt="">
                        <div class="sorting-cover">
                            <span>Happy New Year</span>
                            <!-- <p>Anim pariatur cliche reprehenderit</p> -->
                        </div>
                    </a>
                </li>
                <li class="col-md-3 col-sm-6 col-xs-12 mix category_2" data-cat="2">
                    <a href="">
                        <img class="img-responsive" src="assets/img/main/img8.jpg" alt="">
                        <div class="sorting-cover">
                            <span>Happy New Year</span>
                            <!-- <p>Anim pariatur cliche reprehenderit</p> -->
                        </div>
                    </a>
                </li>
                <li class="col-md-3 col-sm-6 col-xs-12 mix category_1" data-cat="1">
                    <a href="">
                        <img class="img-responsive" src="assets/img/main/img1.jpg" alt="">
                        <div class="sorting-cover">
                            <span>Happy New Year</span>
                           <!--  <p>Anim pariatur cliche reprehenderit</p> -->
                        </div>
                    </a>
                </li>
            </ul>
        
            <div class="clearfix"></div>
        </div>

        

    <!--=== Footer Version 1 ===-->
    <?php include 'footer.php';  ?>

</body>

<!-- Mirrored from htmlstream.com/preview/unify-v1.8/page_home7.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Jan 2016 17:09:16 GMT -->
</html> 