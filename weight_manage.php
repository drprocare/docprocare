<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  

<!-- Mirrored from htmlstream.com/preview/unify-v1.8/page_profile_projects.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Jan 2016 17:11:02 GMT -->
<head>
    <title>Drprocare | Weight Management</title> 

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">

    <!-- Web Fonts -->
    <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin'>

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">

    <!-- CSS Header and Footer -->
    <link rel="stylesheet" href="assets/css/headers/header-default.css">
    <link rel="stylesheet" href="assets/css/footers/footer-v1.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="assets/plugins/animate.css">
    <link rel="stylesheet" href="assets/plugins/line-icons/line-icons.css">
    <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/plugins/scrollbar/css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="assets/plugins/sky-forms-pro/skyforms/css/sky-forms.css">
    <link rel="stylesheet" href="assets/plugins/sky-forms-pro/skyforms/custom/custom-sky-forms.css">

    <!-- CSS Page Style -->
    <link rel="stylesheet" href="assets/css/pages/profile.css">

    <!-- CSS Theme -->
    <link rel="stylesheet" href="assets/css/theme-colors/default.css" id="style_color">
    <link rel="stylesheet" href="assets/css/theme-skins/dark.css">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="assets/css/custom.css">

    <?php include 'head.php'; ?>
</head> 

<body>


<div class="wrapper">
    <!--=== Header ===-->    
    <?php include 'header.php'; ?>
    <!--=== End Header ===-->

    <!--=== Profile ===-->
    <div class="container content profile">
        <div class="row">
            <!--Left Sidebar-->
            
            <!--End Left Sidebar-->

            <!-- Profile Content -->            
            <div class="col-md-12">
                <div class="profile-body">
                    <!--Projects-->
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="easy-block-v1">
                                <img class="img-responsive" src="assets/img/main/w1.jpg" alt="">
                            </div>    
                            <div class="projects">
                                <h2><a class="color-dark" href="#">WEIGHT MANAGEMENT</a></h2>
                               <!--  <ul class="list-unstyled list-inline blog-info-v2">
                                    <li>By: <a class="color-green" href="#">Edward Rooster</a></li>
                                    <li><i class="fa fa-clock-o"></i> Jan 02, 2013</li>
                                </ul> -->
                                <p>Drink Water, Especially Before Meals</p>
                                <br>
                                
                                
                            </div>    
                            <div class="project-share">
                                <ul class="list-inline comment-list-v2 pull-left">
                                    <li><i class="fa fa-thumbs-up"></i> <a href="#">25</a></li>
                                    <li><i class="fa fa-share-alt"></i> <a href="#">32</a></li>
                                    <li><i class="fa fa-star"></i> <a href="#">77</a></li>
                                </ul>    
                                <ul class="list-inline star-vote pull-right">
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star-half-o"></i></li>
                                    <li><i class="color-green fa fa-star-o"></i></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="service-or easy-block-v2 no-margin-bottom">
                                <img class="img-responsive" src="assets/img/main/w14.jpg" alt="">
                                <!-- <div class="easy-bg-v2 rgba-default">Unify</div> -->
                            </div>
                            <div class="projects">
                                <h2><a class="color-dark" href="#">CUT BACK ON ADDED SUGAR</a></h2>
                              <!--   <ul class="list-unstyled list-inline blog-info-v2">
                                    <li>By: <a class="color-green" href="#">Edward Rooster</a></li>
                                    <li><i class="fa fa-clock-o"></i> Jan 07, 2013</li>
                                </ul> -->
                                <p>Added sugar is one of the worst ingredients in the modern diet. Most people consume too much sugar.Studies show that sugar consumption is strongly associated with an increased risk of obesity, as well as conditions including type 2 diabetes and heart disease.</p>
                                <br>
                            </div>    
                            <div class="project-share">
                                <ul class="list-inline comment-list-v2 pull-left">
                                    <li><i class="fa fa-thumbs-up"></i> <a href="#">25</a></li>
                                    <li><i class="fa fa-share-alt"></i> <a href="#">32</a></li>
                                    <li><i class="fa fa-star"></i> <a href="#">77</a></li>
                                </ul>     
                                <ul class="list-inline star-vote pull-right">
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star-half-o"></i></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="service-or easy-block-v2 no-margin-bottom">
                                <img class="img-responsive" src="assets/img/main/w5.jpg" alt="">
                                <!-- <div class="easy-bg-v2 rgba-default">Unify</div> -->
                            </div>
                            <div class="projects">
                                <h2><a class="color-dark" href="#">EAT EGGS FOR BREAKFAST</a></h2>
                               <!--  <ul class="list-unstyled list-inline blog-info-v2">
                                    <li>By: <a class="color-green" href="#">Edward Rooster</a></li>
                                    <li><i class="fa fa-clock-o"></i> Jan 07, 2013</li>
                                </ul> -->
                                <p>It can have all sorts of benefits, including helping you lose weight.Studies show that replacing a grain-based breakfast with eggs can help you eat fewer calories for the next 36 hours as well as lose more weight and body fat.If you don’t eat eggs, that's fine. Any source of quality protein for breakfast should do the trick.</p>
                                <br>
                              
                            </div>    
                            <div class="project-share">
                                 <ul class="list-inline comment-list-v2 pull-left">
                                    <li><i class="fa fa-thumbs-up"></i> <a href="#">25</a></li>
                                    <li><i class="fa fa-share-alt"></i> <a href="#">32</a></li>
                                    <li><i class="fa fa-star"></i> <a href="#">77</a></li>
                                </ul>    
                                <ul class="list-inline star-vote pull-right">
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star-half-o"></i></li>
                                </ul>
                            </div>
                        </div>
                    </div><!--/end row-->
                    <!--End Projects-->

                    <hr>

                    <!--Projects-->
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="easy-block-v1">
                                <img class="img-responsive" src="assets/img/main/w2.jpg" alt="">
                               <!--  <div class="easy-block-v1-badge rgba-purple">Graphic Design</div> -->
                            </div>    
                            <div class="projects">
                                <h2><a class="color-dark" href="#">DRINK GREEN TEA</a></h2>
                               <!--  <ul class="list-unstyled list-inline blog-info-v2">
                                    <li>By: <a class="color-green" href="#">Edward Rooster</a></li>
                                    <li><i class="fa fa-clock-o"></i> Jan 02, 2013</li>
                                </ul> -->
                                <p>Though green tea contains small amounts of caffeine, it is loaded with powerful antioxidants called catechins, which are believed to work synergistically with caffeine to enhance fat burning.Recent studies have shown green tea can potentially have positive effects on everything from weight loss to liver disorders, type 2 diabetes, and Alzheimer's disease.</p>
                                <br>
                               
                            </div>    
                            <div class="project-share">
                                <ul class="list-inline comment-list-v2 pull-left">
                                    <li><i class="fa fa-thumbs-up"></i> <a href="#">25</a></li>
                                    <li><i class="fa fa-share-alt"></i> <a href="#">32</a></li>
                                    <li><i class="fa fa-star"></i> <a href="#">77</a></li>
                                </ul>    
                                <ul class="list-inline star-vote pull-right">
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star-half-o"></i></li>
                                    <li><i class="color-green fa fa-star-o"></i></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="service-or easy-block-v2 no-margin-bottom">
                                <img class="img-responsive" src="assets/img/main/w15.jpg" alt="">
                                <!-- <div class="easy-bg-v2 rgba-blue">Nokia</div> -->
                            </div>
                            <div class="projects">
                                <h2><a class="color-dark" href="#">USE SMALLER PLATES</a></h2>
                                <!-- <ul class="list-unstyled list-inline blog-info-v2">
                                    <li>By: <a class="color-green" href="#">Edward Rooster</a></li>
                                    <li><i class="fa fa-clock-o"></i> Jan 07, 2013</li>
                                </ul> -->
                                <p>Using smaller plates has been shown to help some people automatically eat fewer calories.While studies have proven over and over again that smaller plates lead to smaller portions. The illusion works because we think things are smaller when we compare them to things that are larger.Studies show that refined carbs can spike blood sugar rapidly, leading to hunger, cravings and increased food intake a few hours later. Eating refined carbs is strongly linked to obesity.</p>
                                <br>
                                
                            </div>    
                            <div class="project-share">
                                 <ul class="list-inline comment-list-v2 pull-left">
                                    <li><i class="fa fa-thumbs-up"></i> <a href="#">25</a></li>
                                    <li><i class="fa fa-share-alt"></i> <a href="#">32</a></li>
                                    <li><i class="fa fa-star"></i> <a href="#">77</a></li>
                                </ul>    
                                <ul class="list-inline star-vote pull-right">
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star-half-o"></i></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="service-or easy-block-v2 no-margin-bottom">
                                <img class="img-responsive" src="assets/img/main/w16.jpg" alt="">
                                <!-- <div class="easy-bg-v2 rgba-default">Unify</div> -->
                            </div>
                            <div class="projects">
                                <h2><a class="color-dark" href="#">TRY INTERMITTENT FASTING</a></h2>
                                <!-- <ul class="list-unstyled list-inline blog-info-v2">
                                    <li>By: <a class="color-green" href="#">Edward Rooster</a></li>
                                    <li><i class="fa fa-clock-o"></i> Jan 07, 2013</li>
                                </ul> -->
                                <p>Intermittent fasting iShort-term studies suggest intermittent fasting is as effective for weight loss as continuous calorie restriction.s a popular eating pattern in which people cycle between periods of fasting and eating.Short-term studies suggest intermittent fasting is as effective for weight loss as continuous calorie restriction.Also, it may reduce the loss of muscle mass typically associated with low-calorie diets. However, higher-quality studies are needed before any stronger claims can be made.</p>
                                <br>
                                
                            </div>    
                            <div class="project-share">
                                 <ul class="list-inline comment-list-v2 pull-left">
                                    <li><i class="fa fa-thumbs-up"></i> <a href="#">25</a></li>
                                    <li><i class="fa fa-share-alt"></i> <a href="#">32</a></li>
                                    <li><i class="fa fa-star"></i> <a href="#">77</a></li>
                                </ul>   
                                <ul class="list-inline star-vote pull-right">
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star-half-o"></i></li>
                                </ul>
                            </div>
                        </div>
                    </div><!--/end row-->
                    <!--End Projects-->


                     <hr>

                    <!--Projects-->
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="easy-block-v1">
                                <img class="img-responsive" src="assets/img/main/w17.jpg" alt="">
                                <!-- <div class="easy-block-v1-badge rgba-purple">Graphic Design</div>
 -->                            </div>    
                            <div class="projects">
                                <h2><a class="color-dark" href="#">EAT LESS REFINED CARBS</a></h2>
                                <!-- <ul class="list-unstyled list-inline blog-info-v2">
                                    <li>By: <a class="color-green" href="#">Edward Rooster</a></li>
                                    <li><i class="fa fa-clock-o"></i> Jan 02, 2013</li>
                                </ul> -->
                                <p>Refined carbohydrates include sugar and grains that have been stripped of their fibrous, nutritious parts. These include white bread and pasta.Studies show that refined carbs can spike blood sugar rapidly, leading to hunger, cravings and increased food intake a few hours later. Eating refined carbs is strongly linked to obesity.If you're going to eat carbs, make sure to eat them with their natural fiber.</p>
                                <br>
                               
                            </div>    
                            <div class="project-share">
                                 <ul class="list-inline comment-list-v2 pull-left">
                                    <li><i class="fa fa-thumbs-up"></i> <a href="#">25</a></li>
                                    <li><i class="fa fa-share-alt"></i> <a href="#">32</a></li>
                                    <li><i class="fa fa-star"></i> <a href="#">77</a></li>
                                </ul>    
                                <ul class="list-inline star-vote pull-right">
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star-half-o"></i></li>
                                    <li><i class="color-green fa fa-star-o"></i></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="service-or easy-block-v2 no-margin-bottom">
                                <img class="img-responsive" src="assets/img/main/img18.jpg" alt="">
                               <!--  <div class="easy-bg-v2 rgba-blue">Nokia</div> -->
                            </div>
                            <div class="projects">
                                <h2><a class="color-dark" href="#">GO ON A LOW-CARB DIET</a></h2>
                                <!-- <ul class="list-unstyled list-inline blog-info-v2">
                                    <li>By: <a class="color-green" href="#">Edward Rooster</a></li>
                                    <li><i class="fa fa-clock-o"></i> Jan 07, 2013</li>
                                </ul>
 -->                           <p>If you want to get all the benefits of carb restriction, then consider going all the way and committing to a low-carb diet.Numerous studies show that such a regimen can help you lose 2–3 times as much weight as a standard low-fat diet while also improving your health.</p>
                                <br>
                                
                            </div>    
                            <div class="project-share">
                                <ul class="list-inline comment-list-v2 pull-left">
                                    <li><i class="fa fa-thumbs-up"></i> <a href="#">25</a></li>
                                    <li><i class="fa fa-share-alt"></i> <a href="#">32</a></li>
                                    <li><i class="fa fa-star"></i> <a href="#">77</a></li>
                                </ul>     
                                <ul class="list-inline star-vote pull-right">
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star-half-o"></i></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="service-or easy-block-v2 no-margin-bottom">
                                <img class="img-responsive" src="assets/img/main/w6.jpg" alt="">
                                <!-- <div class="easy-bg-v2 rgba-default">Unify</div> -->
                            </div>
                            <div class="projects">
                                <h2><a class="color-dark" href="#">DRINK COFFEE (PREFERABLY BLACK)</a></h2>
                                <!-- <ul class="list-unstyled list-inline blog-info-v2">
                                    <li>By: <a class="color-green" href="#">Edward Rooster</a></li>
                                    <li><i class="fa fa-clock-o"></i> Jan 07, 2013</li>
                                </ul> -->
                                <p>Coffee has been unfairly demonized. Quality coffee is loaded with antioxidants and can have numerous health benefits.Studies show that the caffeine in coffee can boost metabolism by 3–11% and increase fat burning by up to 10–29 %.Make sure not to add a bunch of sugar or other high-calorie ingredients to your coffee. That will completely negate any benefits.</p>
                                <br>
                              
                            </div>    
                            <div class="project-share">
                                 <ul class="list-inline comment-list-v2 pull-left">
                                    <li><i class="fa fa-thumbs-up"></i> <a href="#">25</a></li>
                                    <li><i class="fa fa-share-alt"></i> <a href="#">32</a></li>
                                    <li><i class="fa fa-star"></i> <a href="#">77</a></li>
                                </ul>    
                                <ul class="list-inline star-vote pull-right">
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star-half-o"></i></li>
                                </ul>
                            </div>
                        </div>
                    </div><!--/end row-->
                    <!--End Projects-->

                     <hr>

                    <!--Projects-->
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="easy-block-v1">
                                <img class="img-responsive" src="assets/img/main/w4.jpg" alt="">
                                <!-- <div class="easy-block-v1-badge rgba-purple">Graphic Design</div> -->
                            </div>    
                            <div class="projects">
                                <h2><a class="color-dark" href="#">EXERCISE PORTION CONTROL OR COUNT CALORIES</a></h2>
                               <!--  <ul class="list-unstyled list-inline blog-info-v2">
                                    <li>By: <a class="color-green" href="#">Edward Rooster</a></li>
                                    <li><i class="fa fa-clock-o"></i> Jan 02, 2013</li>
                                </ul> -->
                                <p>Portion control simply eating less or counting calories can be very useful, for obvious reasons.Some studies shows that keeping a food diary or taking pictures of your meals can help you lose weight.Anything that increases your awareness of what you are eating is likely to be beneficial.</p>
                                <br>
                                
                            </div>    
                            <div class="project-share">
                                <ul class="list-inline comment-list-v2 pull-left">
                                    <li><i class="fa fa-thumbs-up"></i> <a href="#">25</a></li>
                                    <li><i class="fa fa-share-alt"></i> <a href="#">32</a></li>
                                    <li><i class="fa fa-star"></i> <a href="#">77</a></li>
                                </ul>    
                                <ul class="list-inline star-vote pull-right">
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star-half-o"></i></li>
                                    <li><i class="color-green fa fa-star-o"></i></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="service-or easy-block-v2 no-margin-bottom">
                                <img class="img-responsive" src="assets/img/main/w18.jpg" alt="">
                                <!-- <div class="easy-bg-v2 rgba-blue">Nokia</div> -->
                            </div>
                            <div class="projects">
                                <h2><a class="color-dark" href="#">KEEP HEALTHY FOOD AROUND IN CASE YOU GET HUNGRY</a></h2>
                               <!--  <ul class="list-unstyled list-inline blog-info-v2">
                                    <li>By: <a class="color-green" href="#">Edward Rooster</a></li>
                                    <li><i class="fa fa-clock-o"></i> Jan 07, 2013</li>
                                </ul> -->
                                <p>Keeping healthy food nearby can help prevent you from eating something unhealthy if you become excessively hungry.Snacks that are easily portable and simple to prepare include whole fruits, nuts, baby carrots, yogurt and hard-boiled eggs.</p>
                                <br>
                                
                            </div>    
                            <div class="project-share">
                                 <ul class="list-inline comment-list-v2 pull-left">
                                    <li><i class="fa fa-thumbs-up"></i> <a href="#">25</a></li>
                                    <li><i class="fa fa-share-alt"></i> <a href="#">32</a></li>
                                    <li><i class="fa fa-star"></i> <a href="#">77</a></li>
                                </ul>     
                                <ul class="list-inline star-vote pull-right">
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star-half-o"></i></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="service-or easy-block-v2 no-margin-bottom">
                                <img class="img-responsive" src="assets/img/main/w19.jpg" alt="">
                                <!-- div class="easy-bg-v2 rgba-default">Unify</div> -->
                            </div>
                            <div class="projects">
                                <h2><a class="color-dark" href="#">EAT SPICY FOODS</a></h2>
                               <!--  <ul class="list-unstyled list-inline blog-info-v2">
                                    <li>By: <a class="color-green" href="#">Edward Rooster</a></li>
                                    <li><i class="fa fa-clock-o"></i> Jan 07, 2013</li>
                                </ul>
 -->                                <p>Chili peppers contain capsaicin, a spicy compound 
that can boost metabolism and reduce your appetite slightly.However, people may develop 
tolerance to the effects of capsaicin over time, which may limit its long-term effectiveness.</p>
                                <br>
                                
                            </div>    
                            <div class="project-share">
                                 <ul class="list-inline comment-list-v2 pull-left">
                                    <li><i class="fa fa-thumbs-up"></i> <a href="#">25</a></li>
                                    <li><i class="fa fa-share-alt"></i> <a href="#">32</a></li>
                                    <li><i class="fa fa-star"></i> <a href="#">77</a></li>
                                </ul>    
                                <ul class="list-inline star-vote pull-right">
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star-half-o"></i></li>
                                </ul>
                            </div>
                        </div>
                    </div><!--/end row-->
                    <!--End Projects-->


                     <hr>

                    <!--Projects-->
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="easy-block-v1">
                                <img class="img-responsive" src="assets/img/main/w21.jpg" alt="">
                               <!--  <div class="easy-block-v1-badge rgba-purple">Graphic Design</div> -->
                            </div>    
                            <div class="projects">
                                <h2><a class="color-dark" href="#">DO AEROBIC EXERCISE</a></h2>
                                <!-- <ul class="list-unstyled list-inline blog-info-v2">
                                    <li>By: <a class="color-green" href="#">Edward Rooster</a></li>
                                    <li><i class="fa fa-clock-o"></i> Jan 02, 2013</li>
                                </ul> -->
                                <p>Doing aerobic exercise (cardio) is an excellent way to burn calories and improve your physical and mental health.It appears to be particularly effective for losing belly fat, the unhealthy fat that tends to build up around your organs and cause metabolic disease.</p>
                                <br>
                               
                            </div>    
                            <div class="project-share">
                               <ul class="list-inline comment-list-v2 pull-left">
                                    <li><i class="fa fa-thumbs-up"></i> <a href="#">25</a></li>
                                    <li><i class="fa fa-share-alt"></i> <a href="#">32</a></li>
                                    <li><i class="fa fa-star"></i> <a href="#">77</a></li>
                                </ul>   
                                <ul class="list-inline star-vote pull-right">
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star-half-o"></i></li>
                                    <li><i class="color-green fa fa-star-o"></i></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="service-or easy-block-v2 no-margin-bottom">
                                <img class="img-responsive" src="assets/img/main/w20.jpg" alt="">
                                <!-- <div class="easy-bg-v2 rgba-blue">Nokia</div> -->
                            </div>
                            <div class="projects">
                                <h2><a class="color-dark" href="#">LIFT WEIGHTS</a></h2>
                               <!--  <ul class="list-unstyled list-inline blog-info-v2">
                                    <li>By: <a class="color-green" href="#">Edward Rooster</a></li>
                                    <li><i class="fa fa-clock-o"></i> Jan 07, 2013</li>
                                </ul> -->
                                <p>One of the worst side effects of dieting is that it tends to cause muscle loss and metabolic slowdown, often referred to as starvation mode.The best way to prevent this is to do some sort of resistance exercise such as lifting weights. Studies show that weight lifting can help keep your metabolism high and prevent you from losing precious muscle mass.</p>
                                <br>
                                
                            </div>    
                            <div class="project-share">
                                 <ul class="list-inline comment-list-v2 pull-left">
                                    <li><i class="fa fa-thumbs-up"></i> <a href="#">25</a></li>
                                    <li><i class="fa fa-share-alt"></i> <a href="#">32</a></li>
                                    <li><i class="fa fa-star"></i> <a href="#">77</a></li>
                                </ul>    
                                <ul class="list-inline star-vote pull-right">
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star-half-o"></i></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="service-or easy-block-v2 no-margin-bottom">
                                <img class="img-responsive" src="assets/img/main/w22.jpg" alt="">
                               <!--  <div class="easy-bg-v2 rgba-default">Unify</div> -->
                            </div>
                            <div class="projects">
                                <h2><a class="color-dark" href="#">EAT MORE FIBER</a></h2>
                                <!-- <ul class="list-unstyled list-inline blog-info-v2">
                                    <li>By: <a class="color-green" href="#">Edward Rooster</a></li>
                                    <li><i class="fa fa-clock-o"></i> Jan 07, 2013</li>
                                </ul> -->
                                <p>Fiber feeds the helpful bacteria living in our guts, helps keep things moving through the GI tract, can help support cardiovascular health, and can even help support healthy blood sugar levels by slowing the absorption of sugar after a meal. Fiber is often recommended for weight loss.Studies recommends that women ages 19–50 get from 25–28 grams of fiber daily; men ages 19–50 should aim for 30–34 grams each day. Try to get the bulk of your fiber intake from whole food sources like fruits, vegetables, beans, and whole grains. If you want to make extra sure you hit your fiber goal, a supplement can help keep you regular while supporting your digestive health.</p>
                                <br>
                                
                            </div>    
                            <div class="project-share">
                                <ul class="list-inline comment-list-v2 pull-left">
                                    <li><i class="fa fa-thumbs-up"></i> <a href="#">25</a></li>
                                    <li><i class="fa fa-share-alt"></i> <a href="#">32</a></li>
                                    <li><i class="fa fa-star"></i> <a href="#">77</a></li>
                                </ul>    
                                <ul class="list-inline star-vote pull-right">
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star-half-o"></i></li>
                                </ul>
                            </div>
                        </div>
                    </div><!--/end row-->
                    <!--End Projects-->

                     <hr>

                    <!--Projects-->
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="easy-block-v1">
                                <img class="img-responsive" src="assets/img/main/w23.jpg" alt="">
                                <!-- <div class="easy-block-v1-badge rgba-purple">Graphic Design</div> -->
                            </div>    
                            <div class="projects">
                                <h2><a class="color-dark" href="#">EAT MORE VEGETABLES AND FRUITS</a></h2>
                               <!--  <ul class="list-unstyled list-inline blog-info-v2">
                                    <li>By: <a class="color-green" href="#">Edward Rooster</a></li>
                                    <li><i class="fa fa-clock-o"></i> Jan 02, 2013</li>
                                </ul> -->
                                <p>Vegetables and fruits have several properties that make them effective for weight loss.They contain few calories but a lot of fiber. Their high water content gives them low energy density, making them very filling.Studies show that people who eat vegetables and fruits tend to weigh less.These foods are also very nutritious, so eating them is important for your health.</p>
                                <br>
                                
                            </div>    
                            <div class="project-share">
                                <ul class="list-inline comment-list-v2 pull-left">
                                    <li><i class="fa fa-thumbs-up"></i> <a href="#">25</a></li>
                                    <li><i class="fa fa-share-alt"></i> <a href="#">32</a></li>
                                    <li><i class="fa fa-star"></i> <a href="#">77</a></li>
                                </ul>   
                                <ul class="list-inline star-vote pull-right">
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star-half-o"></i></li>
                                    <li><i class="color-green fa fa-star-o"></i></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="service-or easy-block-v2 no-margin-bottom">
                                <img class="img-responsive" src="assets/img/main/w25.jpg" alt="">
                                <!-- <div class="easy-bg-v2 rgba-blue">Nokia</div> -->
                            </div>
                            <div class="projects">
                                <h2><a class="color-dark" href="#">CHEW MORE SLOWLY</a></h2>
                              <!--   <ul class="list-unstyled list-inline blog-info-v2">
                                    <li>By: <a class="color-green" href="#">Edward Rooster</a></li>
                                    <li><i class="fa fa-clock-o"></i> Jan 07, 2013</li>
                                </ul> -->
                                <p>Your brain may take a while to register that you've had enough to eat. Some studies show that chewing more slowly can help you eat fewer calories and increase the production of hormones linked to weight loss.Also consider chewing your food more thoroughly. Studies show that increased chewing may reduce calorie intake at a meal.</p>
                                <br>
                                
                            </div>    
                            <div class="project-share">
                                <ul class="list-inline comment-list-v2 pull-left">
                                    <li><i class="fa fa-thumbs-up"></i> <a href="#">25</a></li>
                                    <li><i class="fa fa-share-alt"></i> <a href="#">32</a></li>
                                    <li><i class="fa fa-star"></i> <a href="#">77</a></li>
                                </ul>     
                                <ul class="list-inline star-vote pull-right">
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star-half-o"></i></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="service-or easy-block-v2 no-margin-bottom">
                                <img class="img-responsive" src="assets/img/main/w24.jpg" alt="">
                               <!--  <div class="easy-bg-v2 rgba-default">Unify</div> -->
                            </div>
                            <div class="projects">
                                <h2><a class="color-dark" href="#">GET GOOD SLEEP</a></h2>
                                <!-- <ul class="list-unstyled list-inline blog-info-v2">
                                    <li>By: <a class="color-green" href="#">Edward Rooster</a></li>
                                    <li><i class="fa fa-clock-o"></i> Jan 07, 2013</li>
                                </ul> -->
                                <p>Sleep is highly underrated but may be just as important as eating healthy and exercising.Studies show that poor sleep is one of the strongest risk factors for obesity, as it’s linked to an 89% increased risk of obesity in children and 55% in adults.</p>
                                <br>
                                
                            </div>    
                            <div class="project-share">
                                 <ul class="list-inline comment-list-v2 pull-left">
                                    <li><i class="fa fa-thumbs-up"></i> <a href="#">25</a></li>
                                    <li><i class="fa fa-share-alt"></i> <a href="#">32</a></li>
                                    <li><i class="fa fa-star"></i> <a href="#">77</a></li>
                                </ul>    
                                <ul class="list-inline star-vote pull-right">
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star-half-o"></i></li>
                                </ul>
                            </div>
                        </div>
                    </div><!--/end row-->
                    <!--End Projects-->

                     <hr>

                    <!--Projects-->
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="easy-block-v1">
                                <img class="img-responsive" src="assets/img/main/w26.jpg" alt="">
                               <!--  <div class="easy-block-v1-badge rgba-purple">Graphic Design</div> -->
                            </div>    
                            <div class="projects">
                                <h2><a class="color-dark" href="#">BEAT YOUR FOOD ADDICTION</a></h2>
                                <!-- <ul class="list-unstyled list-inline blog-info-v2">
                                    <li>By: <a class="color-green" href="#">Edward Rooster</a></li>
                                    <li><i class="fa fa-clock-o"></i> Jan 02, 2013</li>
                                </ul> -->
                                <p>A recent study found that 19.9% of people in North America and Europe fulfill the criteria for food addiction.If you experience overpowering cravings and can't seem to curb your eating no matter how hard you try, you may suffer from addiction.In this case, seek professional help. Trying to lose weight without first combating food addiction is next to impossible.</p>
                                <br>
                                
                            </div>    
                            <div class="project-share">
                                 <ul class="list-inline comment-list-v2 pull-left">
                                    <li><i class="fa fa-thumbs-up"></i> <a href="#">25</a></li>
                                    <li><i class="fa fa-share-alt"></i> <a href="#">32</a></li>
                                    <li><i class="fa fa-star"></i> <a href="#">77</a></li>
                                </ul> 
                                <ul class="list-inline star-vote pull-right">
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star-half-o"></i></li>
                                    <li><i class="color-green fa fa-star-o"></i></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="service-or easy-block-v2 no-margin-bottom">
                                <img class="img-responsive" src="assets/img/main/w10.jpg" alt="">
                                <!-- <div class="easy-bg-v2 rgba-blue">Nokia</div> -->
                            </div>
                            <div class="projects">
                                <h2><a class="color-dark" href="#">EAT MORE PROTEIN</a></h2>
                                <!-- <ul class="list-unstyled list-inline blog-info-v2">
                                    <li>By: <a class="color-green" href="#">Edward Rooster</a></li>
                                    <li><i class="fa fa-clock-o"></i> Jan 07, 2013</li>
                                </ul> -->
                                <p>Protein is the single most important nutrient for losing weight.Eating a high-protein diet has been shown to boost metabolism by 80–100 calories per day while shaving 441 calories per day off your diet.One study also showed that eating 25% of your daily calories as protein reduced obsessive thoughts about food by 60% while cutting desire for late-night snacking in </p>
                                <br>
                                
                            </div>    
                            <div class="project-share">
                                 <ul class="list-inline comment-list-v2 pull-left">
                                    <li><i class="fa fa-thumbs-up"></i> <a href="#">25</a></li>
                                    <li><i class="fa fa-share-alt"></i> <a href="#">32</a></li>
                                    <li><i class="fa fa-star"></i> <a href="#">77</a></li>
                                </ul>     
                                <ul class="list-inline star-vote pull-right">
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star-half-o"></i></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="service-or easy-block-v2 no-margin-bottom">
                                <img class="img-responsive" src="assets/img/main/w27.jpeg" alt="">
                                <!-- <div class="easy-bg-v2 rgba-default">Unify</div>
 -->                            </div>
                            <div class="projects">
                                <h2><a class="color-dark" href="#">DON'T DO SUGARY DRINKS, INCLUDING SODA AND FRUIT JUICE</a></h2>
                                <!-- <ul class="list-unstyled list-inline blog-info-v2">
                                    <li>By: <a class="color-green" href="#">Edward Rooster</a></li>
                                    <li><i class="fa fa-clock-o"></i> Jan 07, 2013</li>
                                </ul> -->
                                <p>Sugar is bad, but sugar in liquid form is even worse. Studies show that calories from liquid sugar may be the single most fattening aspect of the modern diet.For example, one study showed that sugar-sweetened beverages are linked to a 60% increased risk of obesity in children for each daily serving.Keep in mind that this applies to fruit juice as well, which contains a similar amount of sugar as a soft drink like Coke.</p>
                                <br>
                               
                            </div>    
                            <div class="project-share">
                                 <ul class="list-inline comment-list-v2 pull-left">
                                    <li><i class="fa fa-thumbs-up"></i> <a href="#">25</a></li>
                                    <li><i class="fa fa-share-alt"></i> <a href="#">32</a></li>
                                    <li><i class="fa fa-star"></i> <a href="#">77</a></li>
                                </ul>     
                                <ul class="list-inline star-vote pull-right">
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star-half-o"></i></li>
                                </ul>
                            </div>
                        </div>
                    </div><!--/end row-->
                    <!--End Projects-->



                    <hr>

                    <!--Projects-->
                    <div class="row margin-bottom-30">
                        <div class="col-sm-4">
                            <div class="easy-block-v1">
                                <img class="img-responsive" src="assets/img/main/w28.jpg" alt="">
                                <!-- <div class="easy-block-v1-badge rgba-aqua">Unify Project</div> -->
                            </div>    
                            <div class="projects">
                                <h2><a class="color-dark" href="#">EAT WHOLE, SINGLE-INGREDIENT FOODS (REAL FOOD)</a></h2>
                                <!-- <ul class="list-unstyled list-inline blog-info-v2">
                                    <li>By: <a class="color-green" href="#">Edward Rooster</a></li>
                                    <li><i class="fa fa-clock-o"></i> Jan 02, 2013</li>
                                </ul> -->
                                <p>If you want to be a leaner, healthier person, then one of the best things you can do for yourself is to eat whole, single-ingredient foods.These foods are naturally filling, and it's very difficult to gain weight if the majority of your diet is based on them.</p>
                                <br>
                                
                            </div>    
                            <div class="project-share">
                                <ul class="list-inline comment-list-v2 pull-left">
                                    <li><i class="fa fa-thumbs-up"></i> <a href="#">25</a></li>
                                    <li><i class="fa fa-share-alt"></i> <a href="#">32</a></li>
                                    <li><i class="fa fa-star"></i> <a href="#">77</a></li>
                                </ul>    
                                <ul class="list-inline star-vote pull-right">
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star-half-o"></i></li>
                                    <li><i class="color-green fa fa-star-o"></i></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="service-or easy-block-v2 no-margin-bottom">
                                <img class="img-responsive" src="assets/img/main/w29.jpg" alt="">
                                <!-- <div class="easy-bg-v2 rgba-yellow">Sony</div>
 -->                            </div>
                            <div class="projects">
                                <h2><a class="color-dark" href="#">EAT HEALTHY</a></h2>
                               <!--  <ul class="list-unstyled list-inline blog-info-v2">
                                    <li>By: <a class="color-green" href="#">Edward Rooster</a></li>
                                    <li><i class="fa fa-clock-o"></i> Jan 07, 2013</li>
                                </ul> -->
                                <p>One of the biggest problems with diets is that they rarely work in the long term.If anything, people who diet tend to gain more weight over time, and studies show that dieting is a consistent predictor of future weight gain.Instead of going on a diet, aim to become a healthier, happier and fitter person. Focus on nourishing your body instead of depriving it.Weight loss should then follow naturally.</p>
                                <br>
                                
                            </div>    
                            <div class="project-share">
                                  <ul class="list-inline comment-list-v2 pull-left">
                                    <li><i class="fa fa-thumbs-up"></i> <a href="#">25</a></li>
                                    <li><i class="fa fa-share-alt"></i> <a href="#">32</a></li>
                                    <li><i class="fa fa-star"></i> <a href="#">77</a></li>
                                </ul>   
                                <ul class="list-inline star-vote pull-right">
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star-half-o"></i></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="service-or easy-block-v2 no-margin-bottom">
                                <img class="img-responsive" src="assets/img/main/img16.jpg" alt="">
                                <!-- <div class="easy-bg-v2 rgba-default">Unify</div> -->
                            </div>
                            <div class="projects">
                                <h2><a class="color-dark" href="#">Getting Started Photography</a></h2>
                               <!--  <ul class="list-unstyled list-inline blog-info-v2">
                                    <li>By: <a class="color-green" href="#">Edward Rooster</a></li>
                                    <li><i class="fa fa-clock-o"></i> Jan 07, 2013</li>
                                </ul> -->
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry printing. Donec non dignissim eros. Mauris faucibus turpis volutpat sagittis rhoncus. Pellentesque et rhoncus sapien, sed ullamcorper justo.</p>
                                <br>
                               
                            </div>    
                            <div class="project-share">
                                <ul class="list-inline comment-list-v2 pull-left">
                                    <li><i class="fa fa-thumbs-up"></i> <a href="#">25</a></li>
                                    <li><i class="fa fa-share-alt"></i> <a href="#">32</a></li>
                                    <li><i class="fa fa-star"></i> <a href="#">77</a></li>
                                </ul>    
                                <ul class="list-inline star-vote pull-right">
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star"></i></li>
                                    <li><i class="color-green fa fa-star-half-o"></i></li>
                                </ul>
                            </div>
                        </div>
                    </div><!--/end row-->
                    <!--End Projects-->
                    
                </div>
            </div>
            <!-- End Profile Content -->            
        </div>
    </div>
    <!--=== End Profile ===-->

    <!--=== Footer Version 1 ===-->
 <?php include 'footer.php';  ?>


</body>

<!-- Mirrored from htmlstream.com/preview/unify-v1.8/page_profile_projects.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Jan 2016 17:11:02 GMT -->
</html> 