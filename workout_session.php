<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  

<!-- Mirrored from htmlstream.com/preview/unify-v1.8/feature_team_blocks.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Jan 2016 17:14:09 GMT -->
<head>
    <title>Drprocare | Workout Session</title>     

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">

    <!-- Web Fonts -->
    <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin'>

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">

    <!-- CSS Header and Footer -->
    <link rel="stylesheet" href="assets/css/headers/header-default.css">
    <link rel="stylesheet" href="assets/css/footers/footer-v1.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="assets/plugins/animate.css">
    <link rel="stylesheet" href="assets/plugins/line-icons/line-icons.css">
    <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/plugins/cube-portfolio/cubeportfolio/css/cubeportfolio.min.css">
    <link rel="stylesheet" href="assets/plugins/cube-portfolio/cubeportfolio/custom/custom-cubeportfolio.css">

    <!-- CSS Theme -->    
    <link rel="stylesheet" href="assets/css/theme-colors/default.css" id="style_color">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="assets/css/custom.css">

     <?php include 'head.php'; ?>
</head> 

<body>

<div class="wrapper">
    <!--=== Header ===-->    
    <?php include 'header.php'; ?>
    <!--=== End Header ===-->

  

    <!--=== Team v7 ===-->
    <div class="container-fluid" style="margin-top: 10px;">
        <!-- Team Blcoks -->
        <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 team-arrow-right">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        
                        <span class="team-v7-name">Workout session</span>
                       <!--  <span class="team-v7-position">Co-Founder / CEO</span> -->
                        <p>Workout increases your energy level. Exercising delivers oxygen and nutrients to your whole body helping it to work more efficiently and boost your endurance. Exercise makes you happier! Physical activity releases chemicals in your brain called endorphins that are known to make you feel happier and more relaxed. Regular physical activity has been proven to help prevent a wide variety of health problems.</p>

                        <p>That’s why today, we are going to be looking at a 5 day workout routine to get strong and toned.</p>

                        
                       
                       

                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/workout/1.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

        <!-- Team Blcoks -->
        <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 col-md-push-6 team-arrow-left">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name"><h2>Warm Up:</h2></span>                        
                        <!-- <span class="team-v7-position">Co-Founder/ UX Design</span> -->
                        <p> <p>Before you commit to any form of workout routine, it is absolutely vital that you take the time to properly warm up before training.</p>
                        <p>Warming up before training is important because it helps improve your flexibility and mobility, and helps reduce the risk of injury. By stretching the muscles before training, you help to increase muscle fiber elasticity. This is important because it means that the muscle fibers are far less-likely to rip and tear.</p>
                        <p>A good warm up will also increase your core body temperature and metabolism and will potentially improve your athletic performance.</p>
                        <p>Warming up boosts circulation, which in turn means more blood flows around the body. This ensures that more oxygen and nutrients can be carried around the body and be fed to the awaiting muscle cells.</p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-md-pull-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/workout/2.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

         <!-- Team Blcoks -->
        <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 team-arrow-right">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name">Warm Up Routine:</span>
                       <!--  <span class="team-v7-position">Co-Founder / CEO</span> -->
                        <p>A sample warmup routine could consist of the following exercises and movements:</p>

                        <p>1 minute of knee lifts</p>
                        <p>1 minute of heel digs</p>
                        <p>2 sets of 10 shoulder rolls per arm</p>
                        <p>10 knee bends</p>
                        <p>20 head rotations</p>
                        <p>10 hip rotations</p>

                        <p><h3>Monday – Abs</h3></p>

                        <p>1. Sit-Up. 15-20 reps or to failure.</p>
                        <p>2. Flat Bench Lying Leg Raise. 15-20 reps or to failure.</p>
                        <p>3. Jackknife Sit-Up. 15-20 reps or to failure.</p>
                        <p>4. Flat Bench Leg Pull-In. 15-20 reps or to failure.</p>
                        <p>5. Toe Touchers. 15-20 reps (reps or to failure)</p>
                        <p>6. Crunches. 15-20 reps or to failure.</p>
                        <p>7. Reverse Crunch.</p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/workout/3.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

        <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 col-md-push-6 team-arrow-left">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name">Tuesday– Chest and Arms</span>                        
                            
                       <p>  1. Flat bench barbell press – 4 sets of 8 reps</p>
                       <p> 2. Pushups – 4 sets of 10 reps</p>
                       <p> 3. Cable crossovers – 3 sets of 15 reps</p>
                        <p>4. Incline dumbbell flyes – 4 sets of 12 reps</p>
                       <p> 5. Barbell biceps curls – 3 sets of 15 reps</p>
                        <p>6. Alternate arm hammer curls – 4 sets of 12 reps per arm</p>
                       <p> 7. Triceps rope overhead extensions – 3 sets of 20 reps</p>

                        <p><h3>Wednesday – Shoulders and Back</h3></p>
                        <p>1. Standing barbell military press – 4 sets of 10 reps</p>
                        <p>2. Dumbbell lateral raises – 4 sets of 15 reps</p>
                        <p>3. EZ bar upright rows – 3 sets of 15 reps</p>
                        <p>4. Seated dumbbell shoulder press – 4 sets of 10 reps</p>
                        <p>5. Dumbbell shrugs – 4 sets of 10 reps</p>
                        <p>6. Close-grip lat pulldowns – 4 sets of 12 reps</p>
                        <p>7. Dumbbell bent over rows – 4 sets of 12 reps per arm
                        </p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-md-pull-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/workout/4.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

         <!-- Team Blcoks -->
        <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 team-arrow-right">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name">Sarvangasana (Shoulder Stand):</span>
                        <span class="team-v7-position">Co-Founder / CEO</span>
                        <p><h3>Thursday – Cardio Circuit</h3></p>
                            <p>1. 10 burpees</p>
                            <p>2. 10 push ups</p>
                            <p>3. 15 crunches</p>
                            <p>4. 20 squat thrusts</p>
                            <p>5. 3 sets of 10 hanging leg raises</p>
                            <p>6. 3 x 1-minute rounds of plank</p>
                            <p>7. 20 minutes low intensity cardio on the treadmill</p>
                        <p><h3>Friday – Strength day</p></h3>
                           <p> 1. Incline dumbbell press – 5 sets of 5 reps</p>
                            <p>2. Flat bench barbell press – 5 sets of 5 reps</p>
                           <p> 3. Deadlifts – 5 sets of 5 reps</p>
                            <p>4. Barbell clean and press – 5 sets of 5 reps</p>
                           <p> 5. Barbell bent-over rows – 5 sets of 5 reps</p>
                            <p>6. Barbell snatch – 5 sets of 5 reps</p>
                           <p> 7. 10 minutes on the stationary bike</p>
                        <p><h3>Saturday– Legs</h3></p>
                            <p>1. Barbell squats – 4 sets of 8 reps</p>
                            <p>2. Leg press machine – 3 sets of 12 reps</p>
                            <p>3. Leg extensions – 3 sets of 15 reps</p>
                           <p> 4. Hamstring curls – 3 sets of 15 reps</p>
                            <p>5. Walking lunges – 4 sets of 10 reps per leg</p>
                           <p> 6. Seated or standing calf raises – 4 sets of 20 reps per leg</p>
                            <p>7. 10 minutes on the elliptical machine</p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/workout/5.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

    
    </div>
    <!--=== End Team v7 ===-->

    

    <!--=== Footer Version 1 ===-->
   <?php include'footer.php';  ?>
    <!--=== End Footer Version 1 ===-->
</div>




</body>

<!-- Mirrored from htmlstream.com/preview/unify-v1.8/feature_team_blocks.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Jan 2016 17:15:23 GMT -->
</html>